package jwsutil

import (
	"encoding/json"

	"github.com/SermoDigital/jose/jws"
)

// JWSParser parses a JWS and returns a token or errs.
type JWSParser interface {
	Parse([]byte, json.Unmarshaler) (jws.JWS, error)
}

// DefaultJWSParser implements jwsParser
type DefaultJWSParser struct{}

// Parse returns a JWS or an error if it can't parse
func (d DefaultJWSParser) Parse(accessToken []byte, u json.Unmarshaler) (jws.JWS, error) {
	return jws.ParseCompact(accessToken, u)
}
