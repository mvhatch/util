package logutil

import "testing"

func TestNewMultiLogger(t *testing.T) {
	t.Parallel()

	testCases := []struct {
		Name          string
		ExpectedLevel LogLevel
		Loggers       []Logger
	}{
		{
			Name:          "All loggers have OffLevel",
			ExpectedLevel: OffLevel,
			Loggers: []Logger{
				StdOutLogger{Lvl: OffLevel},
				TracerLogger{Lvl: OffLevel},
			},
		},
		{
			Name:          "All loggers have InvalidLogLevel",
			ExpectedLevel: OffLevel,
			Loggers: []Logger{
				StdOutLogger{Lvl: InvalidLevel},
				TracerLogger{Lvl: InvalidLevel},
			},
		},
		{
			Name:          "One loggers is OffLevel, but one is not",
			ExpectedLevel: InfoLevel,
			Loggers: []Logger{
				StdOutLogger{Lvl: OffLevel},
				TracerLogger{Lvl: InfoLevel},
			},
		},
		{
			Name:          "One loggers level is less than another's",
			ExpectedLevel: InfoLevel,
			Loggers: []Logger{
				StdOutLogger{Lvl: ErrorLevel},
				TracerLogger{Lvl: InfoLevel},
			},
		},
	}

	for _, testCase := range testCases {
		t.Run(testCase.Name, func(t *testing.T) {
			multiLogger := NewMultiLogger(testCase.Loggers...)

			if multiLogger.lvl != testCase.ExpectedLevel {
				t.Errorf("expected log level %s, but was %s", testCase.ExpectedLevel, multiLogger.lvl)
			}
		})
	}
}
