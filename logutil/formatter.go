package logutil

import (
	"fmt"
	"strings"
)

// Formatter describes how a log message's output may be formatted
type Formatter func(LogData) string

// KeyValFormatter returns the structured LogData as key=val pairs.
// The output of the pairs is non-deterministic.
func KeyValFormatter(data LogData) string {
	var b strings.Builder
	for key, val := range data {
		fmt.Fprintf(&b, "\n\t%s=%v", key, val)
	}
	return b.String()
}
