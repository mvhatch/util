package logutil

import "fmt"

func ExampleKeyValFormatter() {
	d := LogData{"message": "hello, world!"}
	fmt.Println(KeyValFormatter(d))
	// Output: message=hello, world!
}
