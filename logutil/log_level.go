package logutil

import (
	"fmt"
	"strings"
)

// The different log levels
const (
	OffLevel LogLevel = iota
	DebugLevel
	InfoLevel
	ErrorLevel
)

// InvalidLevel is the value returned when a LogLevel is not a known value
const InvalidLevel LogLevel = -1

// LogLevel represents the different levels available for logging
type LogLevel int8

// String implements Stringer interface
func (l LogLevel) String() string {
	switch l {
	case DebugLevel:
		return "DEBUG"
	case InfoLevel:
		return "INFO"
	case ErrorLevel:
		return "ERROR"
	case OffLevel:
		return "OFF"
	default:
		return "INVALID"
	}
}

//InvalidLogLevel implements errors interface.
type InvalidLogLevel string

// Error implements error interface.
func (e InvalidLogLevel) Error() string {
	return fmt.Sprintf("invalid log level: %s", string(e))
}

// MapLogLevel attempts to convert a string representation of a log level into a LogLevel
func MapLogLevel(level string) (LogLevel, error) {
	switch strings.ToLower(level) {
	case "debug":
		return DebugLevel, nil
	case "info":
		return InfoLevel, nil
	case "error":
		return ErrorLevel, nil
	case "off":
		return OffLevel, nil
	}
	return InvalidLevel, InvalidLogLevel(level)
}

// IsValid returns true if the provided LogLevel is a valid LogLevel
func IsValid(level LogLevel) bool {
	switch level {
	case DebugLevel, InfoLevel, ErrorLevel, OffLevel:
		return true
	}
	return false
}
