package logutil

import (
	"context"
	"fmt"

	"gitlab.com/mvhatch/util/contextutil"

	"github.com/go-stack/stack"
	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
)

var _ Logger = TracerLogger{}

// TracerLogger logs to opentracing spans
type TracerLogger struct {
	BaseLogger
	Lvl         LogLevel
	DefaultData LogData
}

// Debug implements logger interface
func (l TracerLogger) Debug(ctx context.Context, data LogData) {
	l.logToSpan(ctx, DebugLevel, data)
}

// Info implements logger interface
func (l TracerLogger) Info(ctx context.Context, data LogData) {
	l.logToSpan(ctx, InfoLevel, data)
}

// Error implements logger interface
func (l TracerLogger) Error(ctx context.Context, err error) {
	data := LogData{
		levelKey:  ErrorLevel,
		SourceKey: stack.Caller(1),
		"Error":   fmt.Sprintf("%s", err),
	}
	l.logToSpan(ctx, ErrorLevel, data)
}

// Level implements logger interface.
func (l TracerLogger) Level() LogLevel {
	return l.Lvl
}

// Format implements logger interface.
// Opentracing's span logger has its own key: val format defined; this method will not be called.
func (l TracerLogger) Format(data LogData) string {
	return ""
}

// CanLog implements logger interface.
// It returns true if the log level can be logged by this logger, based upon this logger's log level.
func (l TracerLogger) CanLog(lvl LogLevel) bool {
	return l.canLog(l.Lvl, lvl)
}

func (l TracerLogger) logToSpan(ctx context.Context, lvl LogLevel, data LogData) {
	if data == nil {
		return
	}

	if !l.canLog(l.Lvl, lvl) {
		return
	}

	data = l.mergeData(l.DefaultData, data)
	data[levelKey] = lvl
	if src, ok := data[SourceKey]; !ok || src == "" {
		data[SourceKey] = stack.Caller(2)
	}

	if contextID, ok := ctx.Value(contextutil.ID).(string); ok {
		data[contextIDKey] = contextID
	}

	if userID, ok := ctx.Value(contextutil.UserID).(string); ok {
		data[userIDKey] = userID
	}

	if token, ok := contextutil.Token(ctx); ok {
		if tokenID, ok := token.ID(); ok {
			data[jtiKey] = tokenID
		}
	}

	sp := opentracing.SpanFromContext(ctx)
	if sp != nil {
		if lvl == ErrorLevel {
			ext.Error.Set(sp, true)
		}

		for key, val := range data {
			sp.LogKV(key, val)
		}
	}
}
