package logutil

import (
	"fmt"
	"testing"
)

func TestDefaultLogLevel(t *testing.T) {
	t.Parallel()

	var l LogLevel

	if l != OffLevel {
		t.Errorf("expected default log level to be %s, but was %s", OffLevel, l)
	}
}

func TestString(t *testing.T) {
	t.Parallel()

	var invalidLevel LogLevel = -1
	expected := map[LogLevel]string{
		OffLevel:     "OFF",
		DebugLevel:   "DEBUG",
		InfoLevel:    "INFO",
		ErrorLevel:   "ERROR",
		invalidLevel: "INVALID",
	}

	for lvl, val := range expected {
		if lvl.String() != val {
			t.Errorf("expected %s, but was %s", val, lvl.String())
		}
	}
}

func ExampleInvalidLogLevel_Error() {
	var invalidLevel InvalidLogLevel = "FooLevel"

	fmt.Println(invalidLevel.Error())
	// Output: invalid log level: FooLevel
}

func TestMapLogLevel(t *testing.T) {
	t.Parallel()

	cases := map[string]struct {
		LogLevel LogLevel
		Error    error
	}{
		"Debug": {LogLevel: DebugLevel, Error: nil},
		"DEBUG": {LogLevel: DebugLevel, Error: nil},
		"Info":  {LogLevel: InfoLevel, Error: nil},
		"INFO":  {LogLevel: InfoLevel, Error: nil},
		"Error": {LogLevel: ErrorLevel, Error: nil},
		"ERROR": {LogLevel: ErrorLevel, Error: nil},
		"Off":   {LogLevel: OffLevel, Error: nil},
		"OFF":   {LogLevel: OffLevel, Error: nil},
		"Foo":   {LogLevel: InvalidLevel, Error: InvalidLogLevel("Foo")},
	}

	for lvl, expected := range cases {
		mappedLevel, err := MapLogLevel(lvl)

		if mappedLevel != expected.LogLevel {
			t.Errorf("expected mapped level to be %s, but was %s", expected.LogLevel, mappedLevel)
		}

		if err != expected.Error {
			t.Errorf("expected error to be %s, but was %s", expected.Error, err)
		}
	}
}

func TestIsValid(t *testing.T) {
	t.Parallel()

	var custom LogLevel = -1
	cases := map[LogLevel]bool{
		DebugLevel:   true,
		InfoLevel:    true,
		ErrorLevel:   true,
		OffLevel:     true,
		InvalidLevel: false,
		custom:       false,
	}

	for lvl, expected := range cases {
		result := IsValid(lvl)

		if result != expected {
			t.Errorf("expected %s to be %t, but was %t", lvl, expected, result)
		}
	}
}
