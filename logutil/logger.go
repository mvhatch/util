// Package logutil provides some common log utility functions
package logutil

import (
	"context"
	"fmt"
	"log"

	"gitlab.com/mvhatch/util/contextutil"

	"github.com/go-stack/stack"
)

const (
	// SourceKey is the key indicating which source file the log statement generated from
	SourceKey    = "_source"
	levelKey     = "_level"
	contextIDKey = "_contextID"
	userIDKey    = "_userID"
	jtiKey       = "_jti"
)

// Logger describes the behaviors of logging
type Logger interface {
	Level() LogLevel
	CanLog(LogLevel) bool
	Format(LogData) string
	Debug(context.Context, LogData)
	Info(context.Context, LogData)
	Error(context.Context, error)
}

// LogData provides a cheap mechanism for structured logging.
// It is NOT thread safe.
type LogData map[string]interface{}

// BaseLogger implements shared logger functionality that should be embedded in all other loggers.
type BaseLogger struct{}

func (b BaseLogger) canLog(loggerLevel, levelAttempted LogLevel) bool {
	return loggerLevel != OffLevel && IsValid(levelAttempted) && loggerLevel <= levelAttempted
}

// mergeData merges additional log data into a predefined mapping of always-present data.
// It is non-destructive to either log data maps, but keys defined in additional data will
// overwrite the same keys in default data.
func (b BaseLogger) mergeData(defaultData, additionalData LogData) LogData {
	if defaultData == nil {
		return additionalData
	}

	if additionalData == nil {
		return defaultData
	}

	mergedData := LogData{}
	for key, val := range defaultData {
		mergedData[key] = val
	}

	for key, val := range additionalData {
		mergedData[key] = val
	}

	return mergedData
}

var _ Logger = StdOutLogger{}

// StdOutLogger logs to standard out
type StdOutLogger struct {
	BaseLogger
	DefaultData LogData
	Lvl         LogLevel
	Formatter   Formatter
}

// Debug implements Logger interface
func (l StdOutLogger) Debug(ctx context.Context, data LogData) {
	l.log(ctx, DebugLevel, data)
}

// Info implements Logger interface
func (l StdOutLogger) Info(ctx context.Context, data LogData) {
	l.log(ctx, InfoLevel, data)
}

// Error implements Logger interface
func (l StdOutLogger) Error(ctx context.Context, err error) {
	if err == nil {
		return
	}

	data := LogData{
		levelKey:  ErrorLevel,
		SourceKey: stack.Caller(1),
		"Error":   fmt.Sprintf("%+v", err),
	}
	l.log(ctx, ErrorLevel, data)
}

// Level implements Logger interface.
func (l StdOutLogger) Level() LogLevel {
	return l.Lvl
}

// Format implements Logger interface
func (l StdOutLogger) Format(data LogData) string {
	if l.Formatter == nil {
		return ""
	}
	return l.Formatter(data)
}

// CanLog implements Logger interface.
// It returns true if the log level can be logged by this logger, based upon this logger's log level.
func (l StdOutLogger) CanLog(lvl LogLevel) bool {
	return l.canLog(l.Lvl, lvl)
}

func (l StdOutLogger) log(ctx context.Context, lvl LogLevel, data LogData) {
	if data == nil {
		return
	}

	if !l.canLog(l.Lvl, lvl) {
		return
	}

	data = l.mergeData(l.DefaultData, data)
	data[levelKey] = lvl

	if src, ok := data[SourceKey]; !ok || src == "" {
		data[SourceKey] = stack.Caller(2)
	}

	if userID, ok := contextutil.UserID(ctx); ok {
		data[userIDKey] = userID
	}

	if contextID, ok := ctx.Value(contextutil.ID).(string); ok {
		data[contextIDKey] = contextID
	}

	if token, ok := contextutil.Token(ctx); ok {
		if tokenID, ok := token.ID(); ok {
			data[jtiKey] = tokenID
		}
	}

	log.Println(l.Format(data))
}
