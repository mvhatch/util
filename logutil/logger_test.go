package logutil

import (
	"testing"
)

func TestLogger_canLog(t *testing.T) {
	t.Parallel()

	logLevels := []LogLevel{OffLevel, DebugLevel, InfoLevel, ErrorLevel}
	var testLogger StdOutLogger

	for i, currentLogLevel := range logLevels {
		for j, testLevel := range logLevels {
			if i > j && testLogger.canLog(currentLogLevel, testLevel) {
				t.Errorf("logger should not log for %s when set to %s", testLevel, currentLogLevel)
			}
		}
	}
}

func TestLogger_mergeData(t *testing.T) {
	t.Parallel()

	defaultData := LogData{
		"foo": "Foo",
	}

	additionalData := LogData{
		"bar": "Bar",
	}

	var testLogger StdOutLogger
	mergedData := testLogger.mergeData(defaultData, additionalData)

	if len(mergedData) != 2 {
		t.Errorf("expected %d keys in merged data, but was %d", 2, len(mergedData))
	}

	val, ok := mergedData["foo"]
	if !ok {
		t.Error("expected foo key to be present, but was not")
	}

	if val != "Foo" {
		t.Errorf("expected value to be Foo, but was %s", val)
	}

	val, ok = mergedData["bar"]
	if !ok {
		t.Error("expected bar key to be present, but was not")
	}

	if val != "Bar" {
		t.Errorf("expected value to be Bar, but was %s", val)
	}
}

func TestLogger_mergeData_defaultDataNull(t *testing.T) {
	t.Parallel()

	var defaultData LogData
	additionalData := LogData{
		"foo": "Foo",
	}

	var testLogger StdOutLogger
	mergedData := testLogger.mergeData(defaultData, additionalData)

	if len(mergedData) != 1 {
		t.Errorf("expected merged data to have 1 key, but had %d", len(mergedData))
	}

	val, ok := mergedData["foo"]
	if !ok {
		t.Fatal("expected key foo to be present, but was not")
	}

	if val != "Foo" {
		t.Errorf("expected value to be Foo, but was %s", val)
	}
}

func TestLogger_mergeData_additionalDataNull(t *testing.T) {
	t.Parallel()

	defaultData := LogData{
		"foo": "Foo",
	}

	var additionalData LogData
	var testLogger StdOutLogger
	mergedData := testLogger.mergeData(defaultData, additionalData)

	if len(mergedData) != 1 {
		t.Errorf("expected merged data to have 1 key, but had %d", len(mergedData))
	}

	val, ok := mergedData["foo"]
	if !ok {
		t.Fatal("expected key foo to be present, but was not")
	}

	if val != "Foo" {
		t.Errorf("expected value to be Foo, but was %s", val)
	}
}

func TestLogger_mergeData_overwritesDefaultData(t *testing.T) {
	t.Parallel()

	defaultData := LogData{
		"foo": "Foo",
	}
	additionalData := LogData{
		"foo": "Bar",
	}

	var testLogger StdOutLogger
	mergedData := testLogger.mergeData(defaultData, additionalData)

	if len(mergedData) != 1 {
		t.Errorf("expected merged data to have 1 key, but had %d", len(mergedData))
	}

	val, ok := mergedData["foo"]
	if !ok {
		t.Fatal("expected key foo to be present, but was not")
	}

	if val != "Bar" {
		t.Errorf("expected value to be Bar, but was %s", val)
	}

}
