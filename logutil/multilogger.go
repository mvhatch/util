package logutil

import (
	"context"
)

var _ Logger = MultiLogger{}

// MultiLogger aggregates multiple loggers, in order to log a single message to multiple outputs
type MultiLogger struct {
	loggers []Logger
	lvl     LogLevel
}

// NewMultiLogger returns a MultiLogger with the provided loggers set.
func NewMultiLogger(loggers ...Logger) MultiLogger {
	var lvlSet bool
	var lvl LogLevel

	for _, l := range loggers {
		if !IsValid(l.Level()) {
			continue
		}

		if lvlSet == false {
			lvl = l.Level()
		} else if l.Level() < lvl || lvl == OffLevel {
			lvl = l.Level()
		}
	}
	return MultiLogger{loggers: loggers, lvl: lvl}
}

// Debug implements logger interface
func (l MultiLogger) Debug(ctx context.Context, data LogData) {
	for _, l := range l.loggers {
		l.Debug(ctx, data)
	}
}

// Info implements logger interface
func (l MultiLogger) Info(ctx context.Context, data LogData) {
	for _, l := range l.loggers {
		l.Info(ctx, data)
	}
}

// Error implements logger interface
func (l MultiLogger) Error(ctx context.Context, err error) {
	for _, l := range l.loggers {
		l.Error(ctx, err)
	}
}

// Level implements logger interface.
// This logger's level is set to the lowest log level of its aggregated loggers.
func (l MultiLogger) Level() LogLevel {
	return l.lvl
}

// Format implements logger interface.
// This logger has no format of its own, only the underlying loggers do.
func (l MultiLogger) Format(data LogData) string {
	return ""
}

// CanLog implements logger interface.
// It returns true if any of its loggers can log at the provided log level.
func (l MultiLogger) CanLog(lvl LogLevel) bool {
	for _, logger := range l.loggers {
		if logger.CanLog(lvl) {
			return true
		}
	}

	return false
}
