package server

import (
	"context"

	"gitlab.com/mvhatch/util/contextutil"
	"gitlab.com/mvhatch/util/tracingutil"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"github.com/pkg/errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

// ContextInterceptor middleware propagate a a tracing span in the provided context
// for use in a grpc.NewServer call.
//
// For example:
//
//     s := grpc.NewServer(
//         ...,  // (existing ServerOptions)
//         grpc.UnaryInterceptor(otgrpc.OpenTracingServerInterceptor(tracer)))
//
// All gRPC server spans will look for an OpenTracing SpanContext in the gRPC
// metadata; if found, the server span will act as the ChildOf that RPC
// SpanContext.
//
// Root or not, the server Span will be embedded in the context.Context for the
// application-specific gRPC handler(s) to access.
func ContextInterceptor(tracer tracingutil.Tracer) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		md, ok := metadata.FromIncomingContext(ctx)
		if !ok {
			md = metadata.New(nil)
		}
		spanContext, err := tracer.Extract(opentracing.HTTPHeaders, tracingutil.MetadataReaderWriter{MD: md})
		if err != nil && err != opentracing.ErrSpanContextNotFound {
			return nil, errors.Wrap(err, "could not create span context")
		}

		serverSpan := tracer.StartSpan(info.FullMethod, ext.RPCServerOption(spanContext))
		defer serverSpan.Finish()
		ext.Component.Set(serverSpan, tracingutil.GRPCComponent)
		ctx = opentracing.ContextWithSpan(ctx, serverSpan)

		spanContext.ForeachBaggageItem(func(key, val string) bool {
			if key == string(contextutil.ID) {
				ctx = contextutil.AddToContext(ctx, contextutil.ID, val)
			} else if key == string(contextutil.UserIDContextKey) {
				ctx = contextutil.AddToContext(ctx, contextutil.UserIDContextKey, val)
			}
			return true
		})
		return handler(ctx, req)
	}
}
