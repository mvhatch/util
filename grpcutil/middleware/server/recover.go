package server

import (
	"context"

	"gitlab.com/mvhatch/util/logutil"
	"gitlab.com/mvhatch/util/tracingutil"

	"github.com/pkg/errors"
	"google.golang.org/grpc"
)

// RecoveryInterceptor recovers from any panic which may occur during the request's lifetime
func RecoveryInterceptor(tracer tracingutil.Tracer, logger logutil.Logger) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		sp := tracer.StartSpan("RecoveryInterceptor")
		defer sp.Finish()

		defer func() {
			err := recover()
			if err != nil {
				if err, ok := err.(error); ok {
					logger.Error(ctx, errors.Wrap(err, "panic!"))
				}
			}
		}()
		return handler(ctx, req)
	}
}
