// Package server intercepts all requests and serves as middleware
// updating the request or response as necessary (e.g. set response headers, add logging, create a request context, etc.)
package server

import (
	"context"

	"gitlab.com/mvhatch/util/logutil"
	"gitlab.com/mvhatch/util/tracingutil"

	"github.com/opentracing/opentracing-go"
	"github.com/pkg/errors"
	"google.golang.org/grpc"
)

// RouterMiddleware is a mapping of gRPC route paths to their associated Middleware funcs
var RouterMiddleware = make(map[string]Middleware)

// Middleware is a func that is ran before the requested gRPC handler is invoked.
// If the middleware function returns an error, then the requested gRPC handler will not be called and
// the request will abort early with the error.
type Middleware func(context.Context, tracingutil.Tracer, logutil.Logger, interface{}) error

// RouteInterceptor intercepts all requests and checks if the gRPC method to be invoked has a chain of
// interceptors defined for that route.
func RouteInterceptor(tracer tracingutil.Tracer, logger logutil.Logger) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		sp, ctx := opentracing.StartSpanFromContext(ctx, "RouteInterceptor")
		defer sp.Finish()

		routeChain, ok := RouterMiddleware[info.FullMethod]
		if ok {
			if err := routeChain(ctx, tracer, logger, req); err != nil {
				return nil, errors.Wrap(err, "could not execute request")
			}
		}
		return handler(ctx, req)
	}
}
