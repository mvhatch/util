package server

import (
	"context"
	"testing"

	"gitlab.com/mvhatch/util/contextutil"
	"gitlab.com/mvhatch/util/tracingutil"
)

func TestRequestIdHandler(t *testing.T) {
	t.Parallel()

	ctx := context.Background()
	var contextID string

	testRequestInterceptor := RequestIDInterceptor(tracingutil.NoopTracer{})
	_, _ = testRequestInterceptor(ctx, 1, nil, func(ctx context.Context, req interface{}) (interface{}, error) {
		reqID, _ := ctx.Value(contextutil.ID).(string)
		contextID = reqID
		return nil, nil
	})

	if contextID == "" {
		t.Error("expected context id to have value, but did not")
	}
}

func TestRequestIdHandler_doesNotOverrideID(t *testing.T) {
	t.Parallel()

	contextID := "fooID"
	ctx := context.WithValue(context.Background(), contextutil.ID, contextID)
	var newContextID string

	testRequestInterceptor := RequestIDInterceptor(tracingutil.NoopTracer{})
	_, _ = testRequestInterceptor(ctx, 1, nil, func(ctx context.Context, req interface{}) (interface{}, error) {
		reqID, _ := ctx.Value(contextutil.ID).(string)
		newContextID = reqID
		return nil, nil
	})

	if contextID != newContextID {
		t.Errorf("expected context id to be %s, but did was %s", contextID, newContextID)
	}
}
