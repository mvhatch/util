// Package server contains server-side gRPC middleware
package server

import (
	"context"
	"fmt"
	"strings"

	"gitlab.com/mvhatch/util/contextutil"
	"gitlab.com/mvhatch/util/logutil"
	"gitlab.com/mvhatch/util/tracingutil"

	"github.com/pkg/errors"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// ErrorInterceptor intercepts a server's response and unwraps a wrapped error, returning a gRPC error instead.
func ErrorInterceptor(tracer tracingutil.Tracer, logger logutil.Logger) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		sp := tracer.StartSpan("ErrorInterceptor")
		defer sp.Finish()

		resp, err = handler(ctx, req)
		if err != nil {
			logger.Error(ctx, err)
		}
		return resp, toGRPCError(ctx, err)
	}
}

// toGRPCError converts a known error type to a gRPC status code
func toGRPCError(ctx context.Context, err error) error {
	err = errors.Cause(err)
	if err == nil {
		return nil
	}

	reqID, _ := contextutil.ContextID(ctx)

	if err == context.Canceled {
		return status.Error(codes.Canceled, fmt.Sprintf("[%s]: client canceled.", reqID))
	}

	if st, ok := status.FromError(err); ok {
		switch st.Code() {
		case codes.Canceled:
			return status.Error(st.Code(), fmt.Sprintf("[%s]: client canceled.", reqID))
		case codes.InvalidArgument:
			var sb strings.Builder
			sb.WriteString(fmt.Sprintf("[%s]: invalid request. ", reqID))
			for _, detail := range st.Details() {
				switch t := detail.(type) {
				case *errdetails.BadRequest:
					for _, violation := range t.GetFieldViolations() {
						sb.WriteString(fmt.Sprintf("Invalid field %s. %s", violation.GetField(), violation.GetDescription()))
					}
				}
			}
			return status.Error(st.Code(), sb.String())
		case codes.DeadlineExceeded:
			return status.Error(st.Code(), fmt.Sprintf("[%s]: request timed out.", reqID))
		case codes.NotFound:
			return status.Error(st.Code(), fmt.Sprintf("[%s]: resource not found.", reqID))
		case codes.AlreadyExists:
			return status.Error(st.Code(), fmt.Sprintf("[%s]: resource already exists.", reqID))
		case codes.PermissionDenied:
			return status.Error(st.Code(), fmt.Sprintf("[%s]: permission denied.", reqID))
		case codes.ResourceExhausted:
			return status.Error(st.Code(), fmt.Sprintf("[%s]: resource exhausted.", reqID))
		case codes.FailedPrecondition:
			return status.Error(st.Code(), fmt.Sprintf("[%s]: not processable.", reqID))
		case codes.Unauthenticated:
			return status.Error(st.Code(), fmt.Sprintf("[%s]: could not authenticate request.", reqID))
		}
	}

	return status.Error(codes.Internal, fmt.Sprintf("[%s]: unexpected error.", reqID))
}
