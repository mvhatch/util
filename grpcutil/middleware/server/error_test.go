package server

import (
	"context"
	"errors"
	"testing"

	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func TestToGRPCError_nilError(t *testing.T) {
	t.Parallel()

	if err := toGRPCError(context.Background(), nil); err != nil {
		t.Error("expected err to be nil, but was not")
	}
}

func TestToGRPCError(t *testing.T) {
	t.Parallel()

	cases := map[error]codes.Code{
		errors.New("foo"):                                   codes.Internal,
		status.New(codes.DeadlineExceeded, "timeout").Err(): codes.DeadlineExceeded,
	}

	for err, expected := range cases {
		actual := toGRPCError(context.Background(), err)
		s, ok := status.FromError(actual)

		if !ok {
			t.Errorf("expected status.Error but was type %T", actual)
		}
		if s.Code() != expected {
			t.Errorf("expected code %d, but was %d", expected, s.Code())
		}
	}
}
