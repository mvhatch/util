package server

import (
	"context"

	"gitlab.com/mvhatch/util/contextutil"
	"gitlab.com/mvhatch/util/tracingutil"

	"google.golang.org/grpc"
)

// RequestIDInterceptor adds a unique request id to each request if no id already exists.
func RequestIDInterceptor(tracer tracingutil.Tracer) grpc.UnaryServerInterceptor {
	return func(ctx context.Context, req interface{}, info *grpc.UnaryServerInfo, handler grpc.UnaryHandler) (resp interface{}, err error) {
		sp := tracer.StartSpan("RequestIDInterceptor")
		defer sp.Finish()

		if _, ok := contextutil.ContextID(ctx); !ok {
			ctx = contextutil.NewContextWithID(ctx)
		}
		return handler(ctx, req)
	}
}
