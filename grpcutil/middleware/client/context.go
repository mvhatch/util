// Package client contains gRPC client-side middleware
package client

import (
	"context"

	"gitlab.com/mvhatch/util/contextutil"
	"gitlab.com/mvhatch/util/tracingutil"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	"google.golang.org/grpc"
	"google.golang.org/grpc/metadata"
)

// ContextInterceptor propagates a context into a gRPC span
// for use in a grpc.Dial call.
//
// For example:
//
//     conn, err := grpc.Dial(
//         address,
//         ...,  // (existing DialOptions)
//         grpc.WithUnaryInterceptor(otgrpc.OpenTracingClientInterceptor(tracer)))
//
// All gRPC client spans will inject the OpenTracing SpanContext into the gRPC
// metadata; they will also look in the context.Context for an active
// in-process parent Span and establish a ChildOf reference if such a parent
// Span could be found.
func ContextInterceptor(tracer tracingutil.Tracer) grpc.UnaryClientInterceptor {
	return func(ctx context.Context, method string, req, resp interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
		var parentCtx opentracing.SpanContext
		if parent := opentracing.SpanFromContext(ctx); parent != nil {
			parentCtx = parent.Context()
		}
		clientSpan := tracer.StartSpan(method, opentracing.ChildOf(parentCtx), ext.SpanKindRPCClient)
		defer clientSpan.Finish()

		contextID, ok := contextutil.ContextID(ctx)
		if !ok {
			ctx = contextutil.NewContextWithID(ctx)
			contextID, _ = contextutil.ContextID(ctx)
		}
		clientSpan.SetBaggageItem(string(contextutil.ID), contextID)
		ext.Component.Set(clientSpan, tracingutil.GRPCComponent)

		userID, ok := contextutil.UserID(ctx)
		if ok {
			clientSpan.SetBaggageItem(string(contextutil.UserIDContextKey), userID)
		}

		md, ok := metadata.FromOutgoingContext(ctx)
		if !ok {
			md = metadata.New(nil)
		} else {
			md = md.Copy()
		}
		mdWriter := tracingutil.MetadataReaderWriter{MD: md}
		if err := tracer.Inject(clientSpan.Context(), opentracing.HTTPHeaders, mdWriter); err != nil {
			return err
		}
		ctx = metadata.NewOutgoingContext(ctx, md)
		return invoker(ctx, method, req, resp, cc, opts...)
	}
}
