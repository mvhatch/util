package grpcutil

import (
	"context"

	"gitlab.com/mvhatch/util/grpcutil/middleware/client"
	"gitlab.com/mvhatch/util/tracingutil"

	"github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/pkg/errors"
	"google.golang.org/grpc"
)

// possible ClientConnection attempt errors
var (
	ErrServerAddress = errors.New("server address missing")
	ErrNoTracer      = errors.New("tracer missing")
)

//ClientConnectionOptions are the parameters required to establish a gRPC client connection to a gRPC server
type ClientConnectionOptions struct {
	// TODO: some of these fields aren't necessary?
	// ServiceName is the name of the gRPC client service
	ServiceName string
	// ServiceVersion is the version of the gRPC client service
	ServiceVersion string
	// ServerAddress is the host:port name of the gRPC server
	ServerAddress string
	// Optional. If not provided, one will be provided for you.
	Tracer tracingutil.Tracer
}

// NewClientConnection returns a pointer to a grpc.ClientConn or an error if unable to dial the server or the
// ClientConnectionOptions are invalid.
func NewClientConnection(opts ClientConnectionOptions) (*grpc.ClientConn, error) {
	//if opts.ServiceName == "" {
	//	return nil, errors.Wrap(ErrServiceName, "could not create client connection")
	//}
	//
	//if opts.ServiceVersion == "" {
	//	return nil, errors.Wrap(ErrServiceVersion, "could not create client connection")
	//}
	//
	if opts.ServerAddress == "" {
		return nil, errors.Wrap(ErrServerAddress, "could not create client connection")
	}

	if opts.Tracer == nil {
		return nil, errors.Wrap(ErrNoTracer, "could not create client connection")
	}

	clientConn, err := grpc.Dial(
		opts.ServerAddress,
		grpc.WithInsecure(),
		grpc.WithUnaryInterceptor(
			ChainUnaryClient(
				grpc_prometheus.UnaryClientInterceptor,
				client.ContextInterceptor(opts.Tracer),
			),
		),
	)
	if err != nil {
		return nil, errors.Wrap(err, "could not create client connection")
	}

	return clientConn, nil
}

// ChainUnaryClient creates a single interceptor out of a chain of many interceptors.
//
// Execution is done in left-to-right order, including passing of context.
// For example ChainUnaryClient(one, two, three) will execute one before two before three.
func ChainUnaryClient(interceptors ...grpc.UnaryClientInterceptor) grpc.UnaryClientInterceptor {
	n := len(interceptors)

	if n > 1 {
		lastI := n - 1
		return func(ctx context.Context, method string, req, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
			var (
				chainHandler grpc.UnaryInvoker
				curI         int
			)

			chainHandler = func(currentCtx context.Context, currentMethod string, currentReq, currentRepl interface{}, currentConn *grpc.ClientConn, currentOpts ...grpc.CallOption) error {
				if curI == lastI {
					return invoker(currentCtx, currentMethod, currentReq, currentRepl, currentConn, currentOpts...)
				}
				curI++
				err := interceptors[curI](currentCtx, currentMethod, currentReq, currentRepl, currentConn, chainHandler, currentOpts...)
				curI--
				return err
			}

			return interceptors[0](ctx, method, req, reply, cc, chainHandler, opts...)
		}
	}

	if n == 1 {
		return interceptors[0]
	}

	// n == 0; Dummy interceptor maintained for backward compatibility to avoid returning nil.
	return func(ctx context.Context, method string, req, reply interface{}, cc *grpc.ClientConn, invoker grpc.UnaryInvoker, opts ...grpc.CallOption) error {
		return invoker(ctx, method, req, reply, cc, opts...)
	}
}

// WithUnaryClientChain is a grpc.Server config option that accepts multiple unary interceptors.
// Basically syntactic sugar.
func WithUnaryClientChain(interceptors ...grpc.UnaryClientInterceptor) grpc.UnaryClientInterceptor {
	return ChainUnaryClient(interceptors...)
}
