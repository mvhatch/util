package dbutil

import "testing"

func TestSQLDB_FilterValues(t *testing.T) {
	t.Parallel()

	db := &SQLDB{filteredValues: make(map[int]bool)}

	db.FilterValues(0, 1, 4)

	if !db.filteredValues[0] {
		t.Error("expected index 0 to be marked as filtered, but was not")
	}

	if !db.filteredValues[1] {
		t.Error("expected index 1 to be marked as fitered, but was not")
	}

	if !db.filteredValues[4] {
		t.Error("expected index 4 to be marked as filtered, but was not")
	}
}

func TestNewSQLDB_filterValues(t *testing.T) {
	t.Parallel()

	db := &SQLDB{filteredValues: make(map[int]bool)}

	db.FilterValues(0, 1)

	filteredValues := db.filterValues("value1", true, 3)

	if filteredValues[0] != filteredValue {
		t.Errorf("expected %s, but was %s", filteredValue, filteredValues[0])
	}

	if filteredValues[1] != filteredValue {
		t.Errorf("expected %s, but was %s", filteredValue, filteredValues[1])
	}

	if filteredValues[2] != 3 {
		t.Errorf("expected 3, but was %s", filteredValues[2])
	}
}
