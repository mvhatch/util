package dbutil

import "testing"

func TestPaginatedRequest_Limit(t *testing.T) {
	t.Parallel()

	var pr PaginatedNextRequest

	if pr.Limit() != defaultLimit+1 {
		t.Errorf("expected limit to be %d, but was %d", defaultLimit+1, pr.Limit())
	}

	pr = PaginatedNextRequest{Size: 0}

	if pr.Limit() != defaultLimit+1 {
		t.Errorf("expected limit to be %d, but was %d", defaultLimit+1, pr.Limit())
	}

	pr = PaginatedNextRequest{Size: 10}

	if pr.Limit() != 11 {
		t.Errorf("expected limit to be %d, but was %d", 11, pr.Limit())
	}
}

func TestPaginatedRequest_PreviousLimit(t *testing.T) {
	t.Parallel()

	var pr PaginatedPreviousRequest

	if pr.Limit() != defaultLimit {
		t.Errorf("expected previous limit to be %d, but was %d", defaultLimit+1, pr.Limit())
	}

	pr = PaginatedPreviousRequest{Size: 0}

	if pr.Limit() != defaultLimit {
		t.Errorf("expected previous limit to be %d, but was %d", defaultLimit+1, pr.Limit())
	}

	pr = PaginatedPreviousRequest{Size: 10}

	if pr.Limit() != 10 {
		t.Errorf("expected previous limit to be %d, but was %d", 10, pr.Limit())
	}
}

func TestMaxResultsForLimit(t *testing.T) {
	t.Parallel()

	if MaxResultsForLimit(0) != defaultLimit {
		t.Errorf("expected max results %d, but was %d", defaultLimit, MaxResultsForLimit(0))
	}

	if MaxResultsForLimit(10) != 10 {
		t.Errorf("expected max results %d, but was %d", 10, MaxResultsForLimit(10))
	}
}
