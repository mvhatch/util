// Package dbutil provides some helper functions for db use cases
package dbutil

import (
	"context"
	"database/sql"
	"fmt"
	"os"

	"gitlab.com/mvhatch/util/logutil"

	"github.com/go-stack/stack"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq" // import sql driver
	"github.com/pkg/errors"
	"github.com/tanel/dbmigrate"
)

const (
	// filteredValue is replacement text for logging sensitive query data
	filteredValue  = "[FILTERED]"
	queryKey       = "query"
	valuesKey      = "values"
	defaultDriver  = "postgres"
	defaultHost    = "localhost"
	defaultPort    = "5432"
	defaultSSLMode = "disable"
)

// SQLDB wraps a *sqlx.db. It logs queries automatically as it executes them.
type SQLDB struct {
	Client         *sqlx.DB
	filteredValues map[int]bool
	logger         logutil.Logger
}

// DBConfig allows for configuring the database connection settings
type DBConfig struct {
	Driver   string
	Host     string
	Port     string
	Username string
	Password string
	Database string
	SSLMode  string
}

// ParseEnv parses the environment variables for db config settings and returns the DBConfig
func ParseEnv() DBConfig {
	driver := defaultDriver
	if os.Getenv("DB_DRIVER") != "" {
		driver = os.Getenv("DB_DRIVER")
	}

	host := defaultHost
	if os.Getenv("DB_HOST") != "" {
		host = os.Getenv("DB_HOST")
	}

	dbPort := defaultPort
	if os.Getenv("DB_PORT") != "" {
		dbPort = os.Getenv("DB_PORT")
	}

	sslMode := defaultSSLMode
	if os.Getenv("DB_SSL_MODE") != "" {
		sslMode = os.Getenv("DB_SSL_MODE")
	}

	return DBConfig{
		Driver:   driver,
		Host:     host,
		Port:     dbPort,
		Username: os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PASSWORD"),
		Database: os.Getenv("DB_NAME"),
		SSLMode:  sslMode,
	}
}

// NewSQLDB returns a pointer to a SQLDB
func NewSQLDB(conf DBConfig, logger logutil.Logger) (*SQLDB, error) {
	if logger == nil {
		return nil, errors.New("nil logger")
	}

	db, err := sqlx.Connect(conf.Driver, fmt.Sprintf("sslmode=%s host=%s port=%s user=%s dbname=%s password=%s", conf.SSLMode, conf.Host, conf.Port, conf.Username, conf.Database, conf.Password))
	if err != nil {
		return nil, errors.Wrap(err, "could not connect to sql db")
	}

	d := SQLDB{Client: db, filteredValues: make(map[int]bool), logger: logger}
	return &d, nil
}

// Migrate migrates a SQL database
func (db *SQLDB) Migrate(pathToMigrations string) error {
	return dbmigrate.Run(db.Client.DB, pathToMigrations)
}

// FilterValues sets the internal mapping of values to be filtered. When a query is to be executed,
// it will log the query and replace any values selected for filtering with a filtered value instead.
// This method must be called before executing a query if values need to be filtered. Provide the indices of the
// values that need to be filtered. It returns itself for chaining.
func (db *SQLDB) FilterValues(indices ...int) *SQLDB {
	for _, index := range indices {
		db.filteredValues[index] = true
	}

	return db
}

// QueryRowxContext logs a query before selecting a row. It returns a *sqlx.Row
func (db *SQLDB) QueryRowxContext(ctx context.Context, query string, args ...interface{}) *sqlx.Row {
	db.logQuery(ctx, query, args...)
	return db.Client.QueryRowxContext(ctx, query, args...)
}

// TxQueryRowxContext logs a query before executing a transaction.
func (db *SQLDB) TxQueryRowxContext(ctx context.Context, tx *sqlx.Tx, query string, args ...interface{}) *sqlx.Row {
	db.logQuery(ctx, query, args...)
	return tx.QueryRowxContext(ctx, query, args...)
}

// QueryxContext logs a query before selecting the rows. It returns a *sqlx.Rows and error.
func (db *SQLDB) QueryxContext(ctx context.Context, query string, args ...interface{}) (*sqlx.Rows, error) {
	db.logQuery(ctx, query, args...)
	return db.Client.QueryxContext(ctx, query, args...)
}

// TxQueryxContext logs a query before executing a transaction. It returns a sql.Result and error.
func (db *SQLDB) TxQueryxContext(ctx context.Context, tx *sqlx.Tx, query string, args ...interface{}) (*sqlx.Rows, error) {
	db.logQuery(ctx, query, args...)
	return tx.QueryxContext(ctx, query, args...)
}

// ExecContext logs a query before executing a query. It returns a sql.Result and error.
func (db *SQLDB) ExecContext(ctx context.Context, query string, args ...interface{}) (sql.Result, error) {
	db.logQuery(ctx, query, args...)
	return db.Client.ExecContext(ctx, query, args...)
}

// TxExecContext logs a query before executing a transaction. It returns a sql.Result and error.
func (db *SQLDB) TxExecContext(ctx context.Context, tx *sqlx.Tx, query string, args ...interface{}) (sql.Result, error) {
	db.logQuery(ctx, query, args...)
	return tx.ExecContext(ctx, query, args...)
}

// SelectContext logs a query before executing a select. It returns an error if malformed query.
func (db *SQLDB) SelectContext(ctx context.Context, dest interface{}, query string, args ...interface{}) error {
	db.logQuery(ctx, query, args...)
	return db.Client.SelectContext(ctx, dest, query, args...)
}

// Get logs a query before getting the query results.
func (db *SQLDB) Get(ctx context.Context, dest interface{}, query string, args ...interface{}) error {
	db.logQuery(ctx, query, args...)
	return db.Client.Get(dest, query, args...)
}

// logQuery logs sql commands and values at Debug level
func (db *SQLDB) logQuery(ctx context.Context, query string, values ...interface{}) {
	logValues := db.filterValues(values...)
	d := logutil.LogData{
		queryKey:          query,
		valuesKey:         logValues,
		logutil.SourceKey: stack.Caller(2),
	}
	db.logger.Debug(ctx, d)
}

func (db *SQLDB) filterValues(values ...interface{}) []interface{} {
	logValues := make([]interface{}, len(values))

	for i, value := range values {
		if _, ok := db.filteredValues[i]; ok {
			logValues[i] = filteredValue
		} else {
			logValues[i] = value
		}
	}

	db.filteredValues = make(map[int]bool) // reset the filtered values after filtering
	return logValues
}
