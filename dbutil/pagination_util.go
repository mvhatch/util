package dbutil

const defaultLimit = 100

// PaginatedNextRequest specifies pagination params for a query that can return the "next" page of results.
type PaginatedNextRequest struct {
	StartID string
	Size    uint64
	SortBy  string
	Sort    string
}

// PaginatedPreviousRequest specifies pagination params for a query that can return the "previous" page of results.
type PaginatedPreviousRequest struct {
	StartID string
	Size    uint64
	SortBy  string
	Sort    string
}

// Limit returns 1 more than the requested size so we know if there are more results going forward to paginate
// (i.e "next" results).
func (p PaginatedNextRequest) Limit() uint64 {
	if p.Size != 0 {
		return p.Size + 1
	}
	return defaultLimit + 1
}

// Limit returns the limit of records that should be returned when doing a "previous" page query.
func (p PaginatedPreviousRequest) Limit() uint64 {
	if p.Size != 0 {
		return p.Size
	}
	return defaultLimit
}

// MaxResultsForLimit returns the maximum number of results that should be returned to a client, based upon
// the limit of the records requested. For example: if a client requested a limit of 10 records, then the max is
// 10. If the client did not specify a limit, then the max is the default limit
func MaxResultsForLimit(limit uint64) int {
	if limit == 0 {
		return defaultLimit
	}

	return int(limit)
}
