package smtputil

import (
	"context"
	"io"

	"github.com/pkg/errors"
	"gopkg.in/gomail.v2"
)

var _ Emailer = &GoMailSender{}

// GoMailSender implements Emailer
type GoMailSender struct {
	dialer   *gomail.Dialer
	closer   io.Closer
	username string
	password string
}

// NewGoMailSenderParams allows for configuring a gomail email sender
type NewGoMailSenderParams struct {
	Host     string
	Port     int
	Username string
	Password string
}

// NewGoMailSender returns a pointer to a NewGoMailSender
func NewGoMailSender(params NewGoMailSenderParams) (*GoMailSender, error) {
	dialer := gomail.NewPlainDialer(params.Host, params.Port, params.Username, params.Password)
	closer, err := dialer.Dial()
	if err != nil {
		return nil, errors.Wrap(err, "could not create email sender")
	}

	return &GoMailSender{dialer: dialer, closer: closer}, nil
}

// MustNewGoMailSender returns a pointer to a new GoMailSender and io.Closer, or panics if misconfigured.
func MustNewGoMailSender(params NewGoMailSenderParams) *GoMailSender {
	emailer, err := NewGoMailSender(params)
	if err != nil {
		panic(err)
	}
	return emailer
}

// SendEmail attempts to send a variadic amount of emails
func (g GoMailSender) SendEmail(ctx context.Context, emails ...Email) error {
	for _, email := range emails {
		msg := gomail.NewMessage()
		msg.SetHeader(from, email.From)
		msg.SetHeader(to, email.To...)
		msg.SetHeader(subject, email.Subject)
		msg.SetBody(email.ContentType, email.Body)

		if len(email.Cc) > 0 {
			msg.SetHeader(cc, email.Cc...)
		}

		if len(email.Bcc) > 0 {
			msg.SetHeader(bcc, email.Bcc...)
		}

		if err := g.dialer.DialAndSend(msg); err != nil {
			return errors.Wrap(err, "could not send email")
		}
	}

	return nil
}

// Close closes resources created by the dialer. It should be called before exiting.
func (g GoMailSender) Close() error {
	if g.closer != nil {
		return g.closer.Close()
	}

	return nil
}
