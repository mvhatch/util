// Package smtputil contains helper utilities to assist in sending emails
package smtputil

import (
	"context"
	"os"
	"strconv"
	"strings"

	"github.com/pkg/errors"
)

const (
	// ContentTypeTextPlain is a text/plain content type for an email
	ContentTypeTextPlain = "text/plain"

	from    = "From"
	to      = "To"
	subject = "Subject"
	cc      = "Cc"
	bcc     = "Bcc"
)

var (
	// ErrSMTPAddress is returned when unable to create an emailer, due to SMTP address missing
	ErrSMTPAddress = errors.New("smtp address missing")
)

// Emailer prescribes the behavior of something which sends emails
type Emailer interface {
	SendEmail(context.Context, ...Email) error
	Close() error
}

// Email represents an email message
type Email struct {
	To          []string
	From        string
	Cc          []string
	Bcc         []string
	Subject     string
	Body        string
	ContentType string
}

// EmailerFromEnv returns an emailer based upon the env settings or errs if misconfigured
func EmailerFromEnv() (Emailer, error) {
	smtpAddr := os.Getenv("SMTP_ADDRESS")
	if smtpAddr == "" {
		return nil, errors.Wrap(ErrSMTPAddress, "could not create emailer from environment")
	}
	smtpSplit := strings.Split(smtpAddr, ":")
	smtpHost := strings.TrimSpace(smtpSplit[0])
	smtpPort := strings.TrimSpace(smtpSplit[1])
	smtpPortVal, err := strconv.Atoi(smtpPort)
	if err != nil {
		return nil, errors.Wrap(err, "could not create emailer from environment")
	}

	emailer, err := NewGoMailSender(NewGoMailSenderParams{
		Host: smtpHost,
		Port: smtpPortVal,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not create emailer from environment")
	}

	return emailer, nil
}

// MustEmailerFromEnv returns an emailer based upon the env settings or panics if misconfigured
func MustEmailerFromEnv() Emailer {
	emailer, err := EmailerFromEnv()
	if err != nil {
		panic(err)
	}

	return emailer
}
