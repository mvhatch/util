package smtputil

import (
	"context"
)

var _ Emailer = MockEmailSender{}

// MockEmailSender is a mock implementation of Emailer
type MockEmailSender struct {
	OnSendEmail func(context.Context, ...Email) error
	OnClose     func() error
}

// SendEmail is a mock implementation of SendEmail. It panics if no implementation is provided.
func (m MockEmailSender) SendEmail(ctx context.Context, emails ...Email) error {
	if m.OnSendEmail != nil {
		return m.OnSendEmail(ctx, emails...)
	}
	panic("no implementation provided for SendEmail")
}

// Close is a mock implementation. It panics if no implementation is provided.
func (m MockEmailSender) Close() error {
	if m.OnClose != nil {
		return m.OnClose()
	}
	panic("no implementation provided for Close")
}
