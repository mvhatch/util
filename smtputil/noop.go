package smtputil

import "context"

var _ Emailer = NoopEmailSender{}

// NoopEmailSender does no operations for all Emailer methods
type NoopEmailSender struct{}

// SendEmail performs no action and returns a nil error
func (n NoopEmailSender) SendEmail(context.Context, ...Email) error {
	return nil
}

// Close performs no action and returns a nil error
func (n NoopEmailSender) Close() error {
	return nil
}
