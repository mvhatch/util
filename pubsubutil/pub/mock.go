package pub

import (
	"context"

	"gitlab.com/mvhatch/util/pubsubutil"
)

var _ Publisher = MockPublisher{}

// MockPublisher is a mock implementation.
// It panics if an invoked is not provided.
type MockPublisher struct {
	OnPublish func(context.Context, *pubsubutil.Payload) error
}

// Publish is a mock implementation. It panics if no implementation is provided.
func (m MockPublisher) Publish(c context.Context, p *pubsubutil.Payload) error {
	if m.OnPublish != nil {
		return m.OnPublish(c, p)
	}
	panic("no implementation provided for Publish")
}
