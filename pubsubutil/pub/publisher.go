package pub

import (
	"context"

	"gitlab.com/mvhatch/util/pubsubutil"
)

// Publisher publishes a payload
type Publisher interface {
	Publish(context.Context, *pubsubutil.Payload) error
}
