package pub

import "testing"

func TestNewGooglePublisher_errs_when_missing_projectID(t *testing.T) {
	t.Parallel()

	p, err := NewGooglePublisher("")
	if err == nil {
		t.Fatal("expected non-nil error")
	}

	if p != nil {
		t.Error("expected publisher to be nil")
	}
}
