package pub

import (
	"context"
	"encoding/json"
	"strings"

	"gitlab.com/mvhatch/util/pubsubutil"

	"cloud.google.com/go/pubsub"
	"github.com/pkg/errors"
)

var _ Publisher = GooglePublisher{}

// GooglePublisher encapsulates a Google pubsub client and topics.
// It publishes topics.
type GooglePublisher struct {
	client *pubsub.Client
}

// NewGooglePublisher attempts to create a new Google PubSub publisher, or errs if misconfigured.
func NewGooglePublisher(projectID string) (*GooglePublisher, error) {
	if strings.TrimSpace(projectID) == "" {
		return nil, errors.New("projectID missing")
	}

	client, err := pubsub.NewClient(context.Background(), projectID)
	if err != nil {
		return nil, errors.Wrap(err, "could not create google publisher")
	}

	return &GooglePublisher{client: client}, nil
}

// Close cleans up the pubsub connection. It should be called before exiting.
func (g GooglePublisher) Close() error {
	if g.client == nil {
		return nil
	}
	return g.client.Close()
}

// Publish publishes a topic with some data. The topic must already exist or it errs.
func (g GooglePublisher) Publish(ctx context.Context, payload *pubsubutil.Payload) error {
	if err := pubsubutil.ValidatePayload(payload); err != nil {
		return errors.Wrap(err, "could not publish payload")
	}

	topic := g.client.Topic(payload.TopicName)
	data, err := json.Marshal(payload)
	if err != nil {
		return errors.Wrap(err, "could not publish payload")
	}

	_ = topic.Publish(ctx, &pubsub.Message{Data: data})
	return nil
}
