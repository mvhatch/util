package sub

import (
	"context"

	"gitlab.com/mvhatch/util/pubsubutil"
)

// SubscribeParams configures how a subscriber subscribes
type SubscribeParams struct {
	SubscriptionName string
	TopicName        string
	EventHandler     func(context.Context, *pubsubutil.Payload) error
}

// Subscriber subscribes to a topic
type Subscriber interface {
	Subscribe(context.Context, SubscribeParams) error
}
