package sub

import (
	"testing"

	"gitlab.com/mvhatch/util/testingutil"
)

func TestNewGoogleSubscriber(t *testing.T) {
	t.Parallel()

	subscriber, err := NewGoogleSubscriber("fooID")

	if err != nil {
		t.Fatal(err)
	}

	if subscriber == nil {
		t.Error("expected non-nil subscriber")
	}
}

func TestNewGoogleSubscriber_errs_if_projectID_missing(t *testing.T) {
	t.Parallel()

	subscriber, err := NewGoogleSubscriber("")

	testingutil.AssertError(t, ErrNoProjectID, err)

	if subscriber != nil {
		t.Error("expected subscriber to be nil, but wasn't")
	}
}
