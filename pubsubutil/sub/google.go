package sub

import (
	"context"
	"fmt"

	"gitlab.com/mvhatch/util/pubsubutil"

	"cloud.google.com/go/pubsub"
	"github.com/pkg/errors"
)

var (
	// ErrNoTopic is an error returned when looking for a topic but it does not exist
	ErrNoTopic = errors.New("topic not found")
	// ErrNoProjectID is an error returned when projectID is missing when creating a new Google subscriber
	ErrNoProjectID = errors.New("projectID missing")
)

var _ Subscriber = GoogleSubscriber{}

// GoogleSubscriber encapsulates a Google pubsub client.
// It subscribes to topics.
type GoogleSubscriber struct {
	client  *pubsub.Client
	subName string
}

// NewGoogleSubscriber attempts to create a new Google pubsub subscriber, or errs if misconfigured.
func NewGoogleSubscriber(projectID string) (*GoogleSubscriber, error) {
	if projectID == "" {
		return nil, errors.Wrap(ErrNoProjectID, "could not create a new Google subscriber")
	}

	client, err := pubsub.NewClient(context.Background(), projectID)
	if err != nil {
		return nil, errors.Wrap(err, "could not create google subscriber")
	}

	return &GoogleSubscriber{client: client}, nil
}

// CreateSubscription returns a subscription to a topic or errs if misconfigured.
func (g GoogleSubscriber) CreateSubscription(ctx context.Context, subName string, conf pubsub.SubscriptionConfig) (*pubsub.Subscription, error) {
	sub, err := g.client.CreateSubscription(ctx, subName, conf)
	if err != nil {
		return nil, errors.Wrap(err, "could not create subscription")
	}

	return sub, nil
}

// Subscription creates a reference to a subscription.
func (g GoogleSubscriber) Subscription(subName string) *pubsub.Subscription {
	return g.client.Subscription(subName)
}

// Subscribe attempts to retrieve a subscription and listen for messages.
// It errs if the subscription topic does not exist.
// This method blocks.
func (g GoogleSubscriber) Subscribe(ctx context.Context, params SubscribeParams) error {
	sub := g.client.Subscription(params.SubscriptionName)
	ok, err := sub.Exists(ctx)
	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("could not subscribe to %s", params.TopicName))
	}

	if !ok {
		_, err := g.CreateSubscription(ctx, params.SubscriptionName, pubsub.SubscriptionConfig{
			Topic: g.client.Topic(params.TopicName),
		})
		if err != nil {
			return errors.Wrap(err, fmt.Sprintf("could not subscribe to %s", params.TopicName))
		}
	}

	err = sub.Receive(ctx, func(ctx context.Context, m *pubsub.Message) {
		err := params.EventHandler(ctx, &pubsubutil.Payload{
			ID:          m.ID,
			Data:        m.Data,
			Attributes:  m.Attributes,
			PublishTime: m.PublishTime,
		})
		if err != nil {
			m.Nack()
			return
		}

		m.Ack()
	})

	if err != nil {
		return errors.Wrap(err, fmt.Sprintf("could not subscribe to %s", params.SubscriptionName))
	}

	return nil
}
