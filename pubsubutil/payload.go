package pubsubutil

import (
	"fmt"
	"strings"
	"time"

	"github.com/pkg/errors"
)

var (
	// ErrNilPayload is returned when the payload is nil
	ErrNilPayload = errors.New("nil payload")
	// ErrMissingTopic is returned when a payload does not have a topic
	ErrMissingTopic = errors.New("payload topic missing")
)

// ValidatePayload validates that a Payload is ready to be published.
func ValidatePayload(payload *Payload) error {
	if payload == nil {
		return errors.Wrap(ErrNilPayload, "invalid payload")
	}

	if strings.TrimSpace(payload.TopicName) == "" {
		return errors.Wrap(ErrMissingTopic, "invalid payload")
	}

	return nil
}

// Payload encapsulates a topic and piece of data to be published by a publisher.
type Payload struct {
	// ID identifies this message.
	// This ID is assigned by the server and is populated for Messages obtained from a subscription.
	// This field is read-only.
	ID string

	// Data is the actual data in the message.
	Data []byte

	// Attributes represents the key-value pairs the current message
	// is labelled with.
	Attributes map[string]string

	// The time at which the message was published.
	// This is populated by the server for Messages obtained from a subscription.
	// This field is read-only.
	PublishTime time.Time

	// The topic the payload applies to
	TopicName string
}

// GoString returns the string representation when used with %#v
func (p Payload) GoString() string {
	return fmt.Sprintf("Payload{ID: %s, Data: %s, Attributes: %v, PublishTime: %s, TopicName: %s}",
		p.ID,
		string(p.Data),
		p.Attributes,
		p.PublishTime,
		p.TopicName,
	)
}
