package pubsubutil

import (
	"fmt"
	"testing"
	"time"

	"gitlab.com/mvhatch/util/testingutil"
)

func TestValidatePayload(t *testing.T) {
	t.Parallel()

	if err := ValidatePayload(&Payload{TopicName: "foo"}); err != nil {
		t.Fatal(err)
	}
}

func TestValidatePayload_nil_payload(t *testing.T) {
	t.Parallel()

	err := ValidatePayload(nil)

	testingutil.AssertError(t, ErrNilPayload, err)
}

func TestValidatePayload_missing_topic(t *testing.T) {
	t.Parallel()

	err := ValidatePayload(&Payload{})

	testingutil.AssertError(t, ErrMissingTopic, err)
}

func ExamplePayload_GoString() {
	p := Payload{
		ID:   "1234",
		Data: []byte(`{"name": "testUser", "email": "testUser@email.com"`),
		Attributes: map[string]string{
			"attr1": "foo",
		},
		PublishTime: time.Date(2019, 12, 31, 0, 0, 0, 0, time.UTC),
		TopicName:   "baz",
	}

	fmt.Printf("%#v", p)
	// Output:
	// Payload{ID: 1234, Data: {"name": "testUser", "email": "testUser@email.com", Attributes: map[attr1:foo], PublishTime: 2019-12-31 00:00:00 +0000 UTC, TopicName: baz}
}
