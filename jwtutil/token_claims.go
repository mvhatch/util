package jwtutil

import (
	"encoding/json"
	"fmt"
	"time"

	"gitlab.com/mvhatch/util/errorutil"

	"github.com/SermoDigital/jose/jws"
	"github.com/pkg/errors"
)

// ErrNoKey is an error returned when attempting to retrieve a Claims key that is not present.
type ErrNoKey string

// Error implements errors interface
func (e ErrNoKey) Error() string {
	return fmt.Sprintf("no such key: %s", e)
}

// TokenClaims represents all the claims of a token
type TokenClaims jws.Claims

// Get retrieves the value corresponding with key from the Claims.
func (tc TokenClaims) Get(key string) interface{} {
	return jws.Claims(tc).Get(key)
}

// GetString attempts to get the string value for a given key from the Claims.
// If the key is not present, it returns an error.
// If the value is not a string, it returns an error.
func (tc TokenClaims) GetString(key string) (string, error) {
	val := tc.Get(key)
	if val == nil {
		return "", errors.Wrap(ErrNoKey(key), "could not get string")
	}

	stringVal, ok := val.(string)
	if !ok {
		return "", errors.Wrap(errorutil.ErrUnexpectedType{Expected: "", Got: val}, "could not get string")
	}

	return stringVal, nil
}

// Set sets Claims[key] = val. It'll overwrite without warning.
func (tc TokenClaims) Set(key string, val interface{}) {
	jws.Claims(tc).Set(key, val)
}

// Del removes the value that corresponds with key from the Claims.
func (tc TokenClaims) Del(key string) {
	jws.Claims(tc).Del(key)
}

// Has returns true if a value for the given key exists inside the Claims.
func (tc TokenClaims) Has(key string) bool {
	return jws.Claims(tc).Has(key)
}

// MarshalJSON implements json.Marshaler for Claims.
func (tc TokenClaims) MarshalJSON() ([]byte, error) {
	return jws.Claims(tc).MarshalJSON()
}

// UnmarshalJSON implements json.UnmarshalJSON for Claims.
func (tc *TokenClaims) UnmarshalJSON(b []byte) error {
	claims := map[string]interface{}(*tc)
	if err := json.Unmarshal(b, &claims); err != nil {
		return errors.Wrap(err, "could not unmarshal JSON")
	}

	*tc = TokenClaims(jws.Claims(claims))
	return nil
}

// Base64 implements the Encoder interface.
func (tc TokenClaims) Base64() ([]byte, error) {
	return jws.Claims(tc).Base64()
}

// Issuer retrieves claim "iss" per its type in
// https://tools.ietf.org/html/rfc7519#section-4.1.1
func (tc TokenClaims) Issuer() (string, bool) {
	return jws.Claims(tc).Issuer()
}

// Subject retrieves claim "sub" per its type in
// https://tools.ietf.org/html/rfc7519#section-4.1.2
func (tc TokenClaims) Subject() (string, bool) {
	return jws.Claims(tc).Subject()
}

// Audience retrieves claim "aud" per its type in
// https://tools.ietf.org/html/rfc7519#section-4.1.3
func (tc TokenClaims) Audience() ([]string, bool) {
	return jws.Claims(tc).Audience()
}

// Expiration retrieves claim "exp" per its type in
// https://tools.ietf.org/html/rfc7519#section-4.1.4
func (tc TokenClaims) Expiration() (time.Time, bool) {
	return jws.Claims(tc).Expiration()
}

// NotBefore retrieves claim "nbf" per its type in
// https://tools.ietf.org/html/rfc7519#section-4.1.5
func (tc TokenClaims) NotBefore() (time.Time, bool) {
	return jws.Claims(tc).NotBefore()
}

// IssuedAt retrieves claim "iat" per its type in
// https://tools.ietf.org/html/rfc7519#section-4.1.6
func (tc TokenClaims) IssuedAt() (time.Time, bool) {
	return jws.Claims(tc).IssuedAt()
}

// JWTID retrieves claim "jti" per its type in
// https://tools.ietf.org/html/rfc7519#section-4.1.7
func (tc TokenClaims) JWTID() (string, bool) {
	return jws.Claims(tc).JWTID()
}

// RemoveIssuer deletes claim "iss" from c.
func (tc TokenClaims) RemoveIssuer() {
	jws.Claims(tc).RemoveIssuer()
}

// RemoveSubject deletes claim "sub" from c.
func (tc TokenClaims) RemoveSubject() {
	jws.Claims(tc).RemoveIssuer()
}

// RemoveAudience deletes claim "aud" from c.
func (tc TokenClaims) RemoveAudience() {
	jws.Claims(tc).Audience()
}

// RemoveExpiration deletes claim "exp" from c.
func (tc TokenClaims) RemoveExpiration() {
	jws.Claims(tc).RemoveExpiration()
}

// RemoveNotBefore deletes claim "nbf" from c.
func (tc TokenClaims) RemoveNotBefore() {
	jws.Claims(tc).NotBefore()
}

// RemoveIssuedAt deletes claim "iat" from c.
func (tc TokenClaims) RemoveIssuedAt() {
	jws.Claims(tc).IssuedAt()
}

// RemoveJWTID deletes claim "jti" from c.
func (tc TokenClaims) RemoveJWTID() {
	jws.Claims(tc).RemoveJWTID()
}

// SetIssuer sets the issuer claim
func (tc TokenClaims) SetIssuer(issuer string) {
	jws.Claims(tc).SetIssuer(issuer)
}

// SetSubject sets the subject claim
func (tc TokenClaims) SetSubject(subject string) {
	jws.Claims(tc).SetSubject(subject)
}

// SetAudience sets the audience claim
func (tc TokenClaims) SetAudience(audience ...string) {
	jws.Claims(tc).SetAudience(audience...)
}

// SetExpiration sets the expiration claim
func (tc TokenClaims) SetExpiration(expiration time.Time) {
	jws.Claims(tc).SetExpiration(expiration)
}

// SetNotBefore sets the not before claims
func (tc TokenClaims) SetNotBefore(notBefore time.Time) {
	jws.Claims(tc).SetNotBefore(notBefore)
}

// SetIssuedAt sets the issued at claim
func (tc TokenClaims) SetIssuedAt(issuedAt time.Time) {
	jws.Claims(tc).SetIssuedAt(issuedAt)
}

// SetJWTID sets the jwt id claim
func (tc TokenClaims) SetJWTID(uniqueID string) {
	jws.Claims(tc).SetJWTID(uniqueID)
}

// Claims returns all the claims
func (tc TokenClaims) Claims() jws.Claims {
	return jws.Claims(tc)
}
