package jwtutil

import (
	"encoding/json"
	"time"

	"github.com/pkg/errors"

	"github.com/SermoDigital/jose/crypto"
	"github.com/SermoDigital/jose/jws"
	"github.com/SermoDigital/jose/jwt"
)

// Token is a JWT. It consists of a map of token claims
type Token struct {
	jwt.JWT
	Claims *TokenClaims
}

// NewToken returns a Token
func NewToken(claims *TokenClaims) Token {
	return Token{
		JWT:    jws.NewJWT(jws.Claims(*claims), crypto.SigningMethodHS256),
		Claims: claims,
	}
}

// IsExpired returns true if a token expiry is reached
func (t Token) IsExpired() bool {
	exp, ok := t.Claims.Expiration()
	return ok && time.Now().After(exp)
}

// ID return a token's JWT id and a boolean.
// If the boolean is false, then the jti was not set.
func (t Token) ID() (string, bool) {
	return t.Claims.JWTID()
}

// MarshalJSON implements json marshaller interface
func (t Token) MarshalJSON() ([]byte, error) {
	m := map[string]interface{}(t.Claims.Claims())
	m["is_expired"] = t.IsExpired()
	return json.Marshal(m)
}

// UnmarshalJSON implements json.Unmarshaller interface
func (t *Token) UnmarshalJSON(b []byte) error {
	claims := make(map[string]interface{})
	tokenClaims := TokenClaims(claims)
	err := tokenClaims.UnmarshalJSON(b)
	if err != nil {
		return errors.Wrap(err, "could not unmarshal")
	}

	*t = NewToken(&tokenClaims)
	return nil
}
