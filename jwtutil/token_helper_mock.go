package jwtutil

import (
	"github.com/SermoDigital/jose/crypto"
	"github.com/SermoDigital/jose/jwt"
)

var _ TokenHelper = MockTokenHelper{}

// MockTokenHelper provides a mock implementation of tokenHelper interface
type MockTokenHelper struct {
	OnParseJWT    func(string) (*Token, error)
	OnValidateJWT func(Token, crypto.SigningMethod, ...*jwt.Validator) error
	OnGenerateJWT func(TokenClaims) Token
	OnSerialize   func(Token) ([]byte, error)
}

// ParseJWT provides a mock implementation of interface MockTokenHelper.ParseJWT.
// It panics if not implementation provided.
func (m MockTokenHelper) ParseJWT(s string) (*Token, error) {
	if m.OnParseJWT != nil {
		return m.OnParseJWT(s)
	}
	panic("no implementation provided for ParseJWT")
}

// ValidateJWT provides a mock implementation of interface MockTokenHelper.ValidateJWT.
// It panics if not implementation provided.
func (m MockTokenHelper) ValidateJWT(t Token, sm crypto.SigningMethod, v ...*jwt.Validator) error {
	if m.OnValidateJWT != nil {
		return m.OnValidateJWT(t, sm, v...)
	}
	panic("no implementation provided for ValidateJWT")
}

// GenerateJWT provides a mock implementation of interface MockTokenHelper.GenerateJWT.
// It panics if not implementation provided.
func (m MockTokenHelper) GenerateJWT(claims TokenClaims) Token {
	if m.OnGenerateJWT != nil {
		return m.OnGenerateJWT(claims)
	}
	panic("no implementation provided for GenerateJWT")
}

// Serialize provides a mock implementation of interface MockTokenHelper.Serialize.
// It panics if not implementation provided.
func (m MockTokenHelper) Serialize(t Token) ([]byte, error) {
	if m.OnSerialize != nil {
		return m.OnSerialize(t)
	}
	panic("no implementation provided for Serialize")
}
