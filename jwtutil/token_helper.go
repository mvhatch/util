package jwtutil

import (
	"net/http"
	"strings"
	"time"

	"github.com/SermoDigital/jose/crypto"
	"github.com/SermoDigital/jose/jws"
	"github.com/SermoDigital/jose/jwt"
	"github.com/google/uuid"
	"github.com/pkg/errors"
)

var (
	// ErrNoSecretKey is returned when the token signing key has not been set
	ErrNoSecretKey = errors.New("no host_key in config")

	// ErrTokenParser occurs when unable to parse a JWT from an http request
	ErrTokenParser = errors.New("unable to parse JWT")

	_ TokenHelper = &DefaultTokenHelper{}
)

// TokenHelper is a helper interface to parse and validate a JWT from an http request
type TokenHelper interface {
	ParseJWT(string) (*Token, error)
	ValidateJWT(Token, crypto.SigningMethod, ...*jwt.Validator) error
	GenerateJWT(TokenClaims) Token
	Serialize(Token) ([]byte, error)
}

// DefaultTokenHelper implements TokenHelper interface.
// It signs all generated tokens with the same secret (provided upon struct creation).
type DefaultTokenHelper struct {
	secretTokenKey string
}

// MustNewDefaultTokenHelperParams encapsulates the parameters for creating a new DefaultTokenHelper
type MustNewDefaultTokenHelperParams struct {
	SecretTokenKey string
}

// MustNewDefaultTokenHelper returns a pointer to a DefaultTokenHelper or panics if misconfigured.
func MustNewDefaultTokenHelper(params MustNewDefaultTokenHelperParams) *DefaultTokenHelper {
	if strings.TrimSpace(params.SecretTokenKey) == "" {
		panic("SecretTokenKey empty")
	}

	return &DefaultTokenHelper{secretTokenKey: params.SecretTokenKey}
}

// ParseJWT attempts to parse a JWT from a serialized string representation.
// It implements tokenHelper interface.
func (d DefaultTokenHelper) ParseJWT(serialized string) (*Token, error) {
	t, err := d.parseJWT(serialized)
	return t, errors.Wrap(err, "could not parse JWT")
}

// ValidateJWT attempts to validate a JWT.
// It implements TokenValidator interface.
func (d DefaultTokenHelper) ValidateJWT(t Token, method crypto.SigningMethod, v ...*jwt.Validator) error {
	return errors.Wrap(t.Validate([]byte(d.secretTokenKey), method, v...), "could not validate JWT")
}

// GenerateJWT generates a JWT from the provided TokenClaims.
// If the claims do not include an JTI, one will be generated for you. The issued at time will be set to now.
func (d DefaultTokenHelper) GenerateJWT(claims TokenClaims) Token {
	if _, ok := claims.JWTID(); !ok {
		claims.SetJWTID(uuid.New().String())
	}

	if ok := claims.Has("type"); !ok {
		claims.Set("type", "Bearer")
	}

	claims.SetIssuedAt(time.Now())
	return NewToken(&claims)
}

// Serialize serializes a JWT for transmission across the wire
func (d DefaultTokenHelper) Serialize(t Token) ([]byte, error) {
	if strings.TrimSpace(d.secretTokenKey) == "" {
		return nil, errors.Wrap(ErrNoSecretKey, "could not serialize token")
	}
	b, err := t.Serialize([]byte(d.secretTokenKey))
	return b, errors.Wrap(err, "could not serialize token")
}

// parseJWT returns a pointer to a Token by parsing the serialized representation
// of a JWT signature or returns an error if invalid.
func (d DefaultTokenHelper) parseJWT(serialized string) (*Token, error) {
	parsedJWT, err := jws.ParseJWT([]byte(serialized))
	if err != nil {
		return nil, errors.Wrap(err, "could not parse JWT")
	}

	claims := parsedJWT.Claims()
	tokenClaims := TokenClaims(claims)
	token := Token{JWT: parsedJWT, Claims: &tokenClaims}
	return &token, nil
}

// NewJWTFromRequest returns a pointer to a Token by parsing the request
// for a JWT signature or returns an error if invalid
func NewJWTFromRequest(req *http.Request) (*Token, error) {
	parsedJWT, err := jws.ParseJWTFromRequest(req)
	if err != nil {
		return nil, errors.Wrap(ErrTokenParser, "could not parse JWT from request")
	}

	claims := parsedJWT.Claims()
	tokenClaims := TokenClaims(claims)
	token := Token{JWT: parsedJWT, Claims: &tokenClaims}
	return &token, nil
}
