package jwtutil

import (
	"encoding/json"
	"fmt"
	"testing"
	"time"
)

func ExampleMarshalToken() {
	iss := time.Date(2008, time.November, 10, 23, 0, 0, 0, time.UTC)
	exp := time.Date(2009, time.November, 10, 23, 0, 0, 0, time.UTC)

	claims := TokenClaims{"type": "Bearer"}
	claims.SetIssuer("foo.com")
	claims.SetAudience("foo.com")
	claims.SetExpiration(exp)
	claims.SetIssuedAt(iss)
	claims.SetSubject("user")
	claims.SetJWTID("123456789")

	token := NewToken(&claims)
	b, _ := json.Marshal(token)
	fmt.Printf("%+v", string(b))
	//Output:	{"aud":"foo.com","exp":1257894000,"iat":1226358000,"is_expired":true,"iss":"foo.com","jti":"123456789","sub":"user","type":"Bearer"}
}

func TestToken_UnmarshalJSON(t *testing.T) {
	t.Parallel()

	var actual Token
	b := []byte(`{"aud":"foo.com","exp":1257894000,"iat":1226358000,"iss":"foo.com","jti":"123456789","sub":"user","user":{"id":"5555552"}}`)
	if err := json.Unmarshal(b, &actual); err != nil {
		t.Fatal(err)
	}

	expectedIAT := time.Date(2008, time.November, 10, 23, 0, 0, 0, time.UTC)
	expectedExp := time.Date(2009, time.November, 10, 23, 0, 0, 0, time.UTC)

	actualISS, _ := actual.Claims.Issuer()
	if actualISS != "foo.com" {
		t.Errorf("expected iss to be foo.com, but was %s", actualISS)
	}

	actualAud, _ := actual.Claims.Audience()
	if len(actualAud) != 1 || actualAud[0] != "foo.com" {
		t.Errorf("expected aud to be foo.com, but was %s", actualAud)
	}

	actualExp, _ := actual.Claims.Expiration()
	if !actualExp.Equal(expectedExp) {
		t.Errorf("expected exp to be %s, but was %s", expectedExp, actualExp)
	}

	actualIAT, _ := actual.Claims.IssuedAt()
	if !actualIAT.Equal(expectedIAT) {
		t.Errorf("expected iat to be %s, but was %s", expectedIAT, actualIAT)
	}

	actualSub, _ := actual.Claims.Subject()
	if actualSub != "user" {
		t.Errorf("expected sub to be user, but was %s", actualSub)
	}

	actualJWTID, _ := actual.Claims.JWTID()
	if actualJWTID != "123456789" {
		t.Errorf("expected jid to be %s, but was %s", "123456789", actualJWTID)
	}

	if !actual.IsExpired() {
		t.Error("expected token to be expired, but was not")
	}
}

func TestToken_IsExpired(t *testing.T) {
	t.Parallel()

	exp := time.Date(2009, time.November, 10, 23, 0, 0, 0, time.UTC)
	iss := time.Date(2008, time.November, 10, 23, 0, 0, 0, time.UTC)

	claims := TokenClaims{}
	claims.SetExpiration(exp)
	claims.SetIssuedAt(iss)
	claims.SetJWTID("123456789")
	token := NewToken(&claims)

	if !token.IsExpired() {
		t.Error("expected token to be expired, but was not")
	}
}

func TestToken_IsExpired_false(t *testing.T) {
	t.Parallel()

	exp := time.Now().AddDate(1, 0, 0)
	iss := time.Date(2008, time.November, 10, 23, 0, 0, 0, time.UTC)

	claims := TokenClaims{}
	claims.SetExpiration(exp)
	claims.SetIssuedAt(iss)
	claims.SetJWTID("123456789")
	token := NewToken(&claims)

	if token.IsExpired() {
		t.Error("expected token to not be expired, but was")
	}
}
