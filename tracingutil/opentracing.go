package tracingutil

import (
	"context"
	"io"
	"strings"

	"github.com/opentracing/opentracing-go"
	"google.golang.org/grpc/metadata"
)

const (
	// NetComponent is the component tag for net/http opentracing components
	NetComponent = "net/http"
	// GRPCComponent is the component tag for net/http opentracing components
	GRPCComponent = "gRPC"
	// GatewayComponent is the component tag for gateways
	GatewayComponent = "gateway"
)

var _ Tracer = NoopTracer{}

// Tracer wraps opentracing.tracer interface
type Tracer interface {
	opentracing.Tracer
	StartSpanFromContext(context.Context, string, ...opentracing.StartSpanOption) (opentracing.Span, context.Context)
	StartSpanForGateway(in context.Context, spanName string) (opentracing.Span, context.Context)
	StartSpanForEvent(in context.Context, w io.Writer, spanName string) (opentracing.Span, context.Context)
	StartSpanFromEvent(spanName string, r io.Reader) (opentracing.Span, error)
}

// NoopTracer performs no-op for all methods
type NoopTracer struct {
	opentracing.NoopTracer
}

// StartSpanFromContext is a no-op
func (n NoopTracer) StartSpanFromContext(ctx context.Context, spanName string, opts ...opentracing.StartSpanOption) (opentracing.Span, context.Context) {
	sp := n.StartSpan(spanName)
	return sp, context.Background()
}

// StartSpanForGateway is a no-op
func (n NoopTracer) StartSpanForGateway(in context.Context, spanName string) (opentracing.Span, context.Context) {
	sp := n.StartSpan(spanName)
	return sp, context.Background()
}

// StartSpanForEvent is a no-op
func (n NoopTracer) StartSpanForEvent(in context.Context, w io.Writer, spanName string) (opentracing.Span, context.Context) {
	sp := n.StartSpan(spanName)
	return sp, context.Background()
}

// StartSpanFromEvent is a no-op
func (n NoopTracer) StartSpanFromEvent(spanName string, r io.Reader) (opentracing.Span, error) {
	sp := n.StartSpan(spanName)
	return sp, nil
}

// MetadataReaderWriter satisfies both the opentracing.TextMapReader and
// opentracing.TextMapWriter interfaces.
// It is not thread-safe.
type MetadataReaderWriter struct {
	metadata.MD
}

// Set sets a key-value pair in the metadata.
func (w MetadataReaderWriter) Set(key, val string) {
	// The GRPC HPACK implementation rejects any uppercase keys here.
	//
	// As such, since the HTTP_HEADERS format is case-insensitive anyway, we
	// blindly lowercase the key (which is guaranteed to work in the
	// Inject/Extract sense per the OpenTracing spec).
	key = strings.ToLower(key)
	w.MD[key] = append(w.MD[key], val)
}

// ForeachKey retrieves the values associated with each key in the metadata.
func (w MetadataReaderWriter) ForeachKey(handler func(key, val string) error) error {
	for k, vals := range w.MD {
		for _, v := range vals {
			if err := handler(k, v); err != nil {
				return err
			}
		}
	}

	return nil
}
