// Package tracingutil provides utilities to assist in creating openTracing clients and middlewares
package tracingutil

import (
	"context"
	"io"
	"sync"

	"github.com/opentracing/opentracing-go"
	"github.com/opentracing/opentracing-go/ext"
	jaegercfg "github.com/uber/jaeger-client-go/config"
	jaegerlog "github.com/uber/jaeger-client-go/log"
	"github.com/uber/jaeger-lib/metrics"
)

// protects against race conditions around jaeger's globalTracer
var tracerMux sync.Mutex

var _ Tracer = &JaegerTracer{}

// JaegerTracer encapsulates an opentracing.tracer to prevent access to jaeger's global tracer.
// The global tracer is unsafe against goroutine race conditions.
type JaegerTracer struct {
	opentracing.Tracer
}

// NewJaegerTracer returns a jaeger opentracing client, closer, and potential error
func NewJaegerTracer(svcName string, opts ...jaegercfg.Option) (*JaegerTracer, io.Closer, error) {
	cfg, err := jaegercfg.FromEnv()
	if err != nil {
		return nil, nil, err
	}
	// Example logger and metrics factory. Use github.com/uber/jaeger-client-go/log
	// and github.com/uber/jaeger-lib/metrics respectively to bind to real logging and metrics
	// frameworks.
	jLogger := jaegerlog.StdLogger
	jMetricsFactory := metrics.NullFactory

	opts = append(opts, jaegercfg.Logger(jLogger), jaegercfg.Metrics(jMetricsFactory))

	tracerMux.Lock()
	defer tracerMux.Unlock()
	// Initialize tracer with a logger and a metrics factory
	closer, err := cfg.InitGlobalTracer(svcName, opts...)
	tracer := opentracing.GlobalTracer()
	jt := JaegerTracer{Tracer: tracer}
	return &jt, closer, err
}

// StartSpanFromContext starts and returns a Span with `operationName`, using
// any Span found within `ctx` as a ChildOfRef. If no such parent could be
// found, StartSpanFromContext creates a root (parentless) Span.
//
// The second return value is a context.Context object built around the
// returned Span.
//
// Example usage:
//
//    SomeFunction(ctx context.Context, ...) {
//        sp, ctx := jaegerTracer.StartSpanFromContext(ctx, "SomeFunction")
//        defer sp.Finish()
//        ...
//    }
func (j *JaegerTracer) StartSpanFromContext(ctx context.Context, opName string, opts ...opentracing.StartSpanOption) (opentracing.Span, context.Context) {
	var span opentracing.Span
	if parentSpan := opentracing.SpanFromContext(ctx); parentSpan != nil {
		opts = append(opts, opentracing.ChildOf(parentSpan.Context()))
		span = j.StartSpan(opName, opts...)
	} else {
		span = j.StartSpan(opName, opts...)
	}
	return span, opentracing.ContextWithSpan(ctx, span)
}

// StartSpanForGateway starts a new opentracing context with a gateway=true tag
func (j *JaegerTracer) StartSpanForGateway(in context.Context, spanName string) (opentracing.Span, context.Context) {
	gatewayTag := opentracing.Tag{
		Key:   GatewayComponent,
		Value: true,
	}
	return j.StartSpanFromContext(in, spanName, gatewayTag)
}

// StartSpanForEvent starts a new opentracing span with message bus destination and producer tags.
// This method should be called by services just before publish a message/event.
func (j *JaegerTracer) StartSpanForEvent(in context.Context, w io.Writer, spanName string) (opentracing.Span, context.Context) {
	tags := make(opentracing.Tags)
	tags[string(ext.MessageBusDestination)] = spanName
	tags[string(ext.SpanKind)] = "producer"

	sp, ctx := j.StartSpanFromContext(in, spanName, tags)
	err := sp.Tracer().Inject(sp.Context(), opentracing.Binary, w)
	if err != nil {
		sp, ctx = j.StartSpanFromContext(in, spanName, tags)
	}
	return sp, ctx
}

// StartSpanFromEvent starts a new opentracing span with a tag set to consumer.
// This method should be called by event handlers consuming a published event.
func (j *JaegerTracer) StartSpanFromEvent(spanName string, r io.Reader) (opentracing.Span, error) {
	spCtx, err := j.Extract(opentracing.Binary, r)
	if err != nil {
		return nil, err
	}

	tags := make(opentracing.Tags)
	tags[string(ext.SpanKind)] = "consumer"
	sp := j.StartSpan(spanName, opentracing.FollowsFrom(spCtx), tags)
	return sp, nil
}

// WithTag sets top-level tracer tags
func WithTag(key string, value interface{}) jaegercfg.Option {
	return jaegercfg.Tag(key, value)
}
