package httputil

import (
	"net/http"

	"github.com/gorilla/mux"
)

var (
	// HealthHandler returns 200 status when invoked
	HealthHandler = http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})
)

// MuxRouter wraps a gorilla mux.NewRouter
type MuxRouter struct {
	*mux.Router
}

// NewRouter returns a new mux.Router
func NewRouter() *MuxRouter {
	return &MuxRouter{Router: mux.NewRouter()}
}

// RequestParams returns a requests restful params (e.g. /resource/{id}/... would return the id) as a map[string]string
func (m MuxRouter) RequestParams(r *http.Request) map[string]string {
	return mux.Vars(r)
}
