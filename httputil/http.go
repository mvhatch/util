// Package httputil provides some helper functions for common http use cases
package httputil

import (
	"encoding/json"
	"io"
	"net/http"
	"os"

	"gitlab.com/mvhatch/util/contextutil"
	"gitlab.com/mvhatch/util/errorutil"

	"github.com/pkg/errors"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

const (
	contentTypeKey  = "Content-Type"
	errorJSON       = `{"error": "Unexpected error"}`
	errorKey        = "error"
	jsonDataKey     = "data"
	jsonContentType = "application/json; charset=UTF-8"
)

// APIResponse is returned after ever api request. It contains
// either response data for the request, or error data about a failed request
type APIResponse struct {
	Error       *ErrorResponse `json:"error,omitempty"`
	Data        interface{}    `json:"data,omitempty"`
	Hostname    string         `json:"host_name,omitempty"`
	Path        string         `json:"path,omitempty"`
	InstanceID  string         `json:"instance_id,omitempty"`
	CodeVersion string         `json:"code_version,omitempty"`
	RequestID   string         `json:"request_id"`
}

// HandleErrorCode maps a gRPC error code to an http response
func HandleErrorCode(err error, w http.ResponseWriter, r *http.Request) {
	err = errors.Cause(err)
	s := status.Convert(err)
	switch s.Code() {
	case codes.InvalidArgument:
		BadRequestError(w, r, err)
	case codes.NotFound:
		RecordNotFoundError(w, r)
	case codes.Unauthenticated:
		UnAuthorizedError(w, r, err)
	case codes.AlreadyExists:
		ConflictError(w, r, err)
	case codes.PermissionDenied:
		PermissionDeniedError(w, r, err)
	case codes.DeadlineExceeded:
		TimeoutError(w, r)
	default:
		InternalServerError(w, r)
	}
}

// ErrorResponse is structure that contains pertinent info about an api error
type ErrorResponse struct {
	ID          string   `json:"id,omitempty"`
	Description string   `json:"description,omitempty"`
	Details     []string `json:"details,omitempty"`
}

// SetErrorResponse sets the error response to the provided status and adds a
// list of errors to the api response as well
func SetErrorResponse(msg string, w http.ResponseWriter, r *http.Request, status int, errors ...error) {
	errorMsgs := printErrors(errors...)

	id, _ := contextutil.ContextID(r.Context())
	data := ErrorResponse{
		ID:          id,
		Description: msg,
		Details:     errorMsgs,
	}
	ctx := contextutil.AddToContext(r.Context(), errorKey, &data)
	*r = *r.WithContext(ctx)
	SetResponse(w, r, status)
}

// SetDataResponse takes arbitrary key-value data pairs and adds them to the api response
// sets the api status to the provided value
func SetDataResponse(key, data interface{}, w http.ResponseWriter, r *http.Request, status int) {
	ctx := contextutil.AddToContext(r.Context(), key, data)
	*r = *r.WithContext(ctx)
	SetResponse(w, r, status)
}

// InternalServerError sets the error response to 500
func InternalServerError(w http.ResponseWriter, r *http.Request) {
	SetErrorResponse(http.StatusText(http.StatusInternalServerError), w, r, http.StatusInternalServerError)
}

// BadRequestError sets the error response to 400
func BadRequestError(w http.ResponseWriter, r *http.Request, errors ...error) {
	SetErrorResponse(http.StatusText(http.StatusBadRequest), w, r, http.StatusBadRequest, errors...)
}

// ConflictError sets the error response to 409
func ConflictError(w http.ResponseWriter, r *http.Request, errors ...error) {
	SetErrorResponse(http.StatusText(http.StatusConflict), w, r, http.StatusConflict, errors...)
}

// UnAuthorizedError sets the error response to 401
func UnAuthorizedError(w http.ResponseWriter, r *http.Request, errors ...error) {
	SetErrorResponse(http.StatusText(http.StatusUnauthorized), w, r, http.StatusUnauthorized, errors...)
}

// PermissionDeniedError sets the error response to 403
func PermissionDeniedError(w http.ResponseWriter, r *http.Request, errors ...error) {
	SetErrorResponse(http.StatusText(http.StatusForbidden), w, r, http.StatusForbidden, errors...)
}

// RecordNotFoundError sets the error response to 404
func RecordNotFoundError(w http.ResponseWriter, r *http.Request) {
	SetErrorResponse(http.StatusText(http.StatusNotFound), w, r, http.StatusNotFound, errorutil.ErrNotFound)
}

// TimeoutError sets the error response to 405
func TimeoutError(w http.ResponseWriter, r *http.Request) {
	SetErrorResponse(http.StatusText(http.StatusRequestTimeout), w, r, http.StatusRequestTimeout, errorutil.ErrTimeout)
}

// EntityTooLargeError sets the error response to 413
func EntityTooLargeError(w http.ResponseWriter, r *http.Request) {
	SetErrorResponse(http.StatusText(http.StatusRequestEntityTooLarge), w, r, http.StatusRequestEntityTooLarge)
}

// Ok sets the the api response to 200 status and adds any
// json data supplied for the response as well
func Ok(data interface{}, w http.ResponseWriter, r *http.Request) {
	SetDataResponse(jsonDataKey, data, w, r, http.StatusOK)
}

// CreatedOk sets the api response to 201 status and adds any
// json data supplied for the response as well
func CreatedOk(data interface{}, w http.ResponseWriter, r *http.Request) {
	SetDataResponse(jsonDataKey, data, w, r, http.StatusCreated)
}

// SetResponse sets the api response in the ResponseWriter
func SetResponse(w http.ResponseWriter, r *http.Request, status int) {
	id, _ := contextutil.ContextID(r.Context())
	apiResp := &APIResponse{
		Hostname:    os.Getenv("HTTP_HOST"),
		Path:        os.Getenv("PATH_INFO"),
		InstanceID:  os.Getenv("INSTANCE_ID"),
		CodeVersion: os.Getenv("CURRENT_VERSION_ID"),
		RequestID:   id,
	}
	data := r.Context().Value(jsonDataKey)
	if data != nil {
		apiResp.Data = data
	}

	errs := r.Context().Value(errorKey)
	if errs != nil {
		apiResp.Error = errs.(*ErrorResponse)
	}

	j, err := json.Marshal(apiResp)
	if err != nil {
		//This will only happen if there was a problem converting data into JSON
		//This should not ever happen, but just in case...
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(errorJSON))
	}
	w.Header().Set(contentTypeKey, jsonContentType)
	w.WriteHeader(status)
	w.Write(j)
}

// printErrors takes a list of errors and returns a list of their error messages
func printErrors(errors ...error) []string {
	var errorMsgs []string
	//type error cannot be marshaled; they appear as empty interfaces!
	for i := range errors {
		s, ok := status.FromError(errors[i])
		if ok {
			errorMsgs = append(errorMsgs, s.Message())
		} else {
			errorMsgs = append(errorMsgs, errors[i].Error())
		}
	}

	return errorMsgs
}

// ParseRequest attempts to parse a http request body into a struct representation of the payload.
// If it cannot parse the payload, it sets the http status to 400 and returns an error.
func ParseRequest(w http.ResponseWriter, r *http.Request, i interface{}) error {
	err := json.NewDecoder(r.Body).Decode(i)
	if err != nil && err != io.EOF {
		BadRequestError(w, r, errorutil.ErrInvalidJSON) // side-effect?
		return errors.Wrap(err, "could not parse request")
	}
	return nil
}
