package httputil

import (
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"

	"github.com/pkg/errors"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func TestHandleErrorCode(t *testing.T) {
	t.Parallel()

	req, err := http.NewRequest("POST", "/users", nil)
	if err != nil {
		t.Error(err)
	}

	cases := map[error]int{
		status.Error(codes.InvalidArgument, "bad req"):                         http.StatusBadRequest,
		status.Error(codes.NotFound, "not found"):                              http.StatusNotFound,
		status.Error(codes.Unauthenticated, "unauthenticated"):                 http.StatusUnauthorized,
		status.Error(codes.AlreadyExists, "already exists"):                    http.StatusConflict,
		status.Error(codes.PermissionDenied, "permission denied"):              http.StatusForbidden,
		status.Error(codes.DeadlineExceeded, "deadline exceeded"):              http.StatusRequestTimeout,
		errors.New("unknown"):                                                  http.StatusInternalServerError,
		errors.Wrap(status.Error(codes.InvalidArgument, "bad req"), "bad req"): http.StatusBadRequest,
	}

	for err, expected := range cases {
		rr := httptest.NewRecorder()
		HandleErrorCode(err, rr, req)

		if rr.Code != expected {
			t.Errorf("expected error code %d, but was %d", expected, rr.Code)
		}
	}
}

func TestSetErrorResponse(t *testing.T) {
	t.Parallel()

	req, err := http.NewRequest("POST", "/users", nil)
	if err != nil {
		t.Error(err)
	}

	cases := map[int]int{
		http.StatusNotFound:   http.StatusNotFound,
		http.StatusBadRequest: http.StatusBadRequest,
	}

	for status, expected := range cases {
		rr := httptest.NewRecorder()
		SetErrorResponse("foo", rr, req, status)

		if rr.Code != expected {
			t.Errorf("expected error code %d, but was %d", expected, rr.Code)
		}
	}
}

func TestSetDataResponse(t *testing.T) {
	t.Parallel()

	req, err := http.NewRequest("POST", "/users", nil)
	if err != nil {
		t.Error(err)
	}

	cases := map[int]int{
		http.StatusNotFound:   http.StatusNotFound,
		http.StatusBadRequest: http.StatusBadRequest,
	}

	for status, expected := range cases {
		rr := httptest.NewRecorder()
		SetDataResponse("key", "val", rr, req, status)

		if rr.Code != expected {
			t.Errorf("expected error code %d, but was %d", expected, rr.Code)
		}
	}
}

func TestInternalServerError(t *testing.T) {
	t.Parallel()

	req, err := http.NewRequest("POST", "/users", nil)
	if err != nil {
		t.Error(err)
	}

	rr := httptest.NewRecorder()

	InternalServerError(rr, req)

	if rr.Code != http.StatusInternalServerError {
		t.Errorf("expected error code %d, but was %d", http.StatusInternalServerError, rr.Code)
	}
}

func TestBadRequestError(t *testing.T) {
	t.Parallel()

	req, err := http.NewRequest("POST", "/users", nil)
	if err != nil {
		t.Error(err)
	}

	rr := httptest.NewRecorder()

	BadRequestError(rr, req)

	if rr.Code != http.StatusBadRequest {
		t.Errorf("expected error code %d, but was %d", http.StatusBadRequest, rr.Code)
	}
}

func TestConflictError(t *testing.T) {
	t.Parallel()

	req, err := http.NewRequest("POST", "/users", nil)
	if err != nil {
		t.Error(err)
	}

	rr := httptest.NewRecorder()

	ConflictError(rr, req)

	if rr.Code != http.StatusConflict {
		t.Errorf("expected error code %d, but was %d", http.StatusConflict, rr.Code)
	}
}

func TestUnAuthorizedError(t *testing.T) {
	t.Parallel()

	req, err := http.NewRequest("POST", "/users", nil)
	if err != nil {
		t.Error(err)
	}

	rr := httptest.NewRecorder()

	UnAuthorizedError(rr, req)

	if rr.Code != http.StatusUnauthorized {
		t.Errorf("expected error code %d, but was %d", http.StatusUnauthorized, rr.Code)
	}
}

func TestRecordNotFoundError(t *testing.T) {
	t.Parallel()

	req, err := http.NewRequest("POST", "/users", nil)
	if err != nil {
		t.Error(err)
	}

	rr := httptest.NewRecorder()

	RecordNotFoundError(rr, req)

	if rr.Code != http.StatusNotFound {
		t.Errorf("expected error code %d, but was %d", http.StatusNotFound, rr.Code)
	}
}

func TestEntityTooLargeError(t *testing.T) {
	t.Parallel()

	req, err := http.NewRequest("POST", "/users", nil)
	if err != nil {
		t.Error(err)
	}

	rr := httptest.NewRecorder()

	EntityTooLargeError(rr, req)

	if rr.Code != http.StatusRequestEntityTooLarge {
		t.Errorf("expected error code %d, but was %d", http.StatusRequestEntityTooLarge, rr.Code)
	}
}

func TestOk(t *testing.T) {
	t.Parallel()

	req, err := http.NewRequest("POST", "/users", nil)
	if err != nil {
		t.Error(err)
	}

	rr := httptest.NewRecorder()

	Ok("foo", rr, req)

	if rr.Code != http.StatusOK {
		t.Errorf("expected error code %d, but was %d", http.StatusOK, rr.Code)
	}
}

func TestCreatedOk(t *testing.T) {
	t.Parallel()

	req, err := http.NewRequest("POST", "/users", nil)
	if err != nil {
		t.Error(err)
	}

	rr := httptest.NewRecorder()

	CreatedOk("foo", rr, req)

	if rr.Code != http.StatusCreated {
		t.Errorf("expected error code %d, but was %d", http.StatusCreated, rr.Code)
	}
}

func TestSetResponse(t *testing.T) {
	t.Parallel()

	req, err := http.NewRequest("POST", "/users", nil)
	if err != nil {
		t.Error(err)
	}

	rr := httptest.NewRecorder()

	SetResponse(rr, req, http.StatusCreated)

	if rr.Code != http.StatusCreated {
		t.Errorf("expected error code %d, but was %d", http.StatusCreated, rr.Code)
	}
}

func TestErrorUtil_PrintError(t *testing.T) {
	t.Parallel()

	err1 := errors.New("error 1")
	err2 := errors.New("error 2")

	errorMsgs := printErrors(err1, err2)
	expected := []string{"error 1", "error 2"}

	if len(errorMsgs) != 2 {
		t.Error("Expected 2 error messages returned, not ", len(errorMsgs))
	}

	for i := range expected {
		if errorMsgs[i] != expected[i] {
			t.Errorf("Expected error message: %s, Got: %s", expected[i], errorMsgs[i])
		}
	}
}

func TestParseRequest_error(t *testing.T) {
	t.Parallel()

	rr := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodPost, "http://foo.com", strings.NewReader(`{`))
	var i interface{}

	if err := ParseRequest(rr, req, i); err == nil {
		t.Error("expected a parsing error, but was none")
	}
	if rr.Code != http.StatusBadRequest {
		t.Errorf("expected status %d, but was %d", http.StatusBadRequest, rr.Code)
	}
}

func TestParseRequest(t *testing.T) {
	t.Parallel()

	rr := httptest.NewRecorder()
	req := httptest.NewRequest(http.MethodPost, "http://foo.com", strings.NewReader(`{"foo": "bar"}`))
	i := make(map[string]interface{})

	if err := ParseRequest(rr, req, &i); err != nil {
		t.Fatal(err)
	}
}
