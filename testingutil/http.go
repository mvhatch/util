// Package testingutil provides some helper functions for unit testing
package testingutil

import (
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/http/httptest"
	"testing"
)

// OkHandler turns Ok into a http.Handler
func OkHandler() http.Handler {
	return http.HandlerFunc(Ok)
}

// Ok always responds 200 ok to any requests
func Ok(w http.ResponseWriter, r *http.Request) {
	w.WriteHeader(http.StatusOK)
}

// SendSafeRequest is a helper func that sends an http req and checks for an error
func SendSafeRequest(req *http.Request, t *testing.T) *http.Response {
	res, err := http.DefaultClient.Do(req)

	if err != nil {
		t.Error(err)
	}
	return res
}

// ParseJSON is a helper func that parses a json resp body and returns the map[string]interface{}
func ParseJSON(res *http.Response, t *testing.T, data interface{}) {
	decoder := json.NewDecoder(res.Body)
	err := decoder.Decode(&data)
	if err != nil && err != io.EOF {
		t.Error(err)
	}
	defer res.Body.Close()
}

// TestAuthenticatedGet200 tests that an authenticated GET request to a server path returns a 200 status code.
func TestAuthenticatedGet200(t *testing.T, server *httptest.Server, path string) {
	authenticatedGet(t, server, path, http.StatusOK)
}

// TestGet200 tests that a GET request to a server path returns a 200 status code.
func TestGet200(t *testing.T, server *httptest.Server, path string) {
	get(t, server, path, http.StatusOK)
}

// TestAuthenticatedGet400 tests that an authenticated GET request to a server path returns a 400 status code.
func TestAuthenticatedGet400(t *testing.T, server *httptest.Server, path string) {
	authenticatedRequest(t, server, path, nil, http.MethodGet, http.StatusBadRequest)
}

// TestGet400 tests that a GET request to a server path returns a 400 status code.
func TestGet400(t *testing.T, server *httptest.Server, path string) {
	get(t, server, path, http.StatusBadRequest)
}

// TestGet404 tests that a GET request to a server path returns a 404 status code.
func TestGet404(t *testing.T, server *httptest.Server, path string) {
	get(t, server, path, http.StatusNotFound)
}

// TestAuthenticatedGet404 tests that an authenticated GET request to a server path returns a 404 status code.
func TestAuthenticatedGet404(t *testing.T, server *httptest.Server, path string) {
	authenticatedRequest(t, server, path, nil, http.MethodGet, http.StatusNotFound)
}

// TestAuthenticatedGet500 tests that an authenticated GET request to a server path returns a 500 status code.
func TestAuthenticatedGet500(t *testing.T, server *httptest.Server, path string) {
	authenticatedRequest(t, server, path, nil, http.MethodGet, http.StatusInternalServerError)
}

// TestGet500 tests that a GET request to a server path returns a 500 status code.
func TestGet500(t *testing.T, server *httptest.Server, path string) {
	get(t, server, path, http.StatusInternalServerError)
}

// TestAuthenticatedPost200 tests that an authenticated POST request to a server path returns a 200 status code.
func TestAuthenticatedPost200(t *testing.T, server *httptest.Server, path string, body io.Reader) {
	authenticatedRequest(t, server, path, body, http.MethodPost, http.StatusOK)
}

// TestPost200 tests that a POST request with a given payload to a server path returns a 200 status code.
func TestPost200(t *testing.T, server *httptest.Server, path string, body io.Reader) {
	post(t, server, path, body, http.StatusOK)
}

// TestAuthenticatedPost201 tests that an authenticated POST request to a server path returns a 201 status code.
func TestAuthenticatedPost201(t *testing.T, server *httptest.Server, path string, body io.Reader) {
	authenticatedRequest(t, server, path, body, http.MethodPost, http.StatusCreated)
}

// TestPost201 tests that a POST request with a given payload to a server path returns a 201 status code.
func TestPost201(t *testing.T, server *httptest.Server, path string, body io.Reader) {
	post(t, server, path, body, http.StatusCreated)
}

// TestAuthenticatedPost400 tests that an authenticated POST request to a server path returns a 400 status code.
func TestAuthenticatedPost400(t *testing.T, server *httptest.Server, path string, body io.Reader) {
	authenticatedRequest(t, server, path, body, http.MethodPost, http.StatusBadRequest)
}

// TestPost400 tests that a POST request with a given payload to a server path returns a 400 status code.
func TestPost400(t *testing.T, server *httptest.Server, path string, body io.Reader) {
	post(t, server, path, body, http.StatusBadRequest)
}

// TestAuthenticatedPost401 tests that an authenticated POST request to a server path returns a 401 status code.
func TestAuthenticatedPost401(t *testing.T, server *httptest.Server, path string, body io.Reader) {
	authenticatedRequest(t, server, path, body, http.MethodPost, http.StatusUnauthorized)
}

// TestPost401 tests that a POST request with a given payload to a server path returns a 401 status code.
func TestPost401(t *testing.T, server *httptest.Server, path string, body io.Reader) {
	post(t, server, path, body, http.StatusUnauthorized)
}

// TestAuthenticatedPost403 tests that an authenticated POST request to a server path returns a 403 status code.
func TestAuthenticatedPost403(t *testing.T, server *httptest.Server, path string, body io.Reader) {
	authenticatedRequest(t, server, path, body, http.MethodPost, http.StatusForbidden)
}

// TestPost403 tests that a POST request with a given payload to a server path returns a 403 status code.
func TestPost403(t *testing.T, server *httptest.Server, path string, body io.Reader) {
	post(t, server, path, body, http.StatusForbidden)
}

// TestAuthenticatedPost500 tests that an authenticated POST request to a server path returns a 500 status code.
func TestAuthenticatedPost500(t *testing.T, server *httptest.Server, path string, body io.Reader) {
	authenticatedRequest(t, server, path, body, http.MethodPost, http.StatusInternalServerError)
}

// TestPost500 tests that a POST request with a given payload to a server path returns a 500 status code.
func TestPost500(t *testing.T, server *httptest.Server, path string, body io.Reader) {
	post(t, server, path, body, http.StatusInternalServerError)
}

func post(t *testing.T, server *httptest.Server, path string, body io.Reader, expectedStatus int) {
	defer server.Close()

	resp, err := http.Post(fmt.Sprintf("%s%s", server.URL, path), "application/x-www-form-urlencoded", body)
	if err != nil {
		t.Fatal(err)
	}

	if resp.StatusCode != expectedStatus {
		t.Errorf("expected http status %d, but was %d", expectedStatus, resp.StatusCode)
	}
}

func authenticatedGet(t *testing.T, server *httptest.Server, path string, expectedStatus int) {
	defer server.Close()

	req, err := http.NewRequest(http.MethodGet, fmt.Sprintf("%s%s", server.URL, path), nil)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Authorization", "fooBearer")
	resp, err := server.Client().Do(req)
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != expectedStatus {
		t.Errorf("expected http status %d, but was %d", expectedStatus, resp.StatusCode)
	}
}

func get(t *testing.T, server *httptest.Server, path string, expectedStatus int) {
	defer server.Close()

	resp, err := http.Get(fmt.Sprintf("%s%s", server.URL, path))
	if err != nil {
		t.Fatal(err)
	}

	if resp.StatusCode != expectedStatus {
		t.Errorf("expected http status %d, but was %d", expectedStatus, resp.StatusCode)
	}
}

func authenticatedRequest(t *testing.T, server *httptest.Server, path string, body io.Reader, method string, expectedStatus int) {
	defer server.Close()

	req, err := http.NewRequest(method, fmt.Sprintf("%s%s", server.URL, path), body)
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Authorization", "fooBearer")
	resp, err := server.Client().Do(req)
	if err != nil {
		t.Fatal(err)
	}
	if resp.StatusCode != expectedStatus {
		t.Errorf("expected http status %d, but was %d", expectedStatus, resp.StatusCode)
	}
}

// HTTPTransport implements http.Client::Transport
type HTTPTransport func(req *http.Request) *http.Response

// RoundTrip mocks an http round trip call
func (f HTTPTransport) RoundTrip(req *http.Request) (*http.Response, error) {
	return f(req), nil
}
