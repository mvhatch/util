package testingutil

import (
	"context"
	"fmt"
	"reflect"
	"testing"

	"gitlab.com/mvhatch/util/dbutil"

	"github.com/pkg/errors"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

// AssertError is a helper function that asserts the error value of a root cause of an error (not the error string)
func AssertError(t *testing.T, expected, actual error) {
	t.Helper()

	expected = errors.Cause(expected)
	actual = errors.Cause(actual)

	if expected != actual {
		t.Errorf("expected: %s, but was: %s", expected, actual)
	}
}

// AssertErrorType is a helper function that asserts the Type of a root cause of an error (not the error string)
func AssertErrorType(t *testing.T, expected, actual error) {
	t.Helper()

	expected = errors.Cause(expected)
	actual = errors.Cause(actual)

	if reflect.TypeOf(expected) != reflect.TypeOf(actual) {
		t.Errorf("expected error %T, but was %T", expected, actual)
	}
}

// AssertErrorCode is a helper function that asserts that and error has an expected gRPC error code.
func AssertErrorCode(t *testing.T, err error, expected codes.Code) {
	t.Helper()

	err = errors.Cause(err)
	st, ok := status.FromError(err)
	if !ok {
		t.Fatalf("expected a status erorr, but was %T", err)
	}

	if st.Code() != expected {
		t.Errorf("epxected code %d, but was %d", expected, st.Code())
	}
}

// AssertSlices is a helper function that asserts two slices are the same length and are equal, according to a
// provided equalFn.
func AssertSlices(t *testing.T, expected, actual interface{}, equalFn func(index int, expected, actual interface{}) bool) {
	t.Helper()

	expectedKind := reflect.TypeOf(expected).Kind()
	if expectedKind != reflect.Slice {
		t.Fatalf("expected 'expected' to be a slice but was %T", expected)
	}

	actualKind := reflect.TypeOf(actual).Kind()
	if actualKind != reflect.Slice {
		t.Fatalf("expected 'actual' to be a slice but was %T", actual)
	}

	expectedVal := reflect.ValueOf(expected)
	actualVal := reflect.ValueOf(actual)
	expectedLen := expectedVal.Len()
	actualLen := actualVal.Len()
	if expectedLen != actualLen {
		t.Fatalf("'expected' slice length %d does not equal 'actual' slice length %d", expectedLen, actualLen)
	}

	for i := 0; i < expectedLen; i++ {
		expectedValAtIndex := expectedVal.Index(i).Interface()
		actualValAtIndex := actualVal.Index(i).Interface()

		if ok := equalFn(i, expectedValAtIndex, actualValAtIndex); !ok {
			t.Fatalf("'expected' %v did not match 'actual' %v at index %d", expectedValAtIndex, actualValAtIndex, i)
		}
	}
}

// AssertTableRows asserts the a database table has the provided number of rows
func AssertTableRows(t *testing.T, table string, rows int, sqlClient *dbutil.SQLDB) {
	t.Helper()

	query := fmt.Sprintf("SELECT COUNT(*) FROM %s", table)
	var count int

	row := sqlClient.QueryRowxContext(context.Background(), query)
	if err := row.Scan(&count); err != nil {
		t.Fatal(err)
	}

	if count != rows {
		t.Fatalf("expected %d rows returned, but was %d", rows, count)
	}
}

// AssertPanic asserts that function panics as expected
func AssertPanic(t *testing.T, f func()) {
	t.Helper()

	defer func() {
		err := recover()
		if err == nil {
			t.Error("expected to panic, but did not")
		}
	}()

	f()
}
