// Package errorutil consolidates all errors which can be returned by either http or gRPC services
package errorutil

import (
	"database/sql"
	"fmt"

	"github.com/pkg/errors"
	"google.golang.org/genproto/googleapis/rpc/errdetails"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

var (
	// ErrTimeout is returned when a request takes too long
	ErrTimeout = errors.New("request timed out")

	// ErrInvalidJSON is returned when malformed JSON is encountered
	ErrInvalidJSON = errors.New("invalid JSON")

	// ErrNotFound is returned when a record isn't found.
	ErrNotFound = errors.New("record not found")

	// ErrNoRowsAffected is returned when a query should have affected rows, but did not.
	// Perhaps due to bad data or invalid permissions.
	ErrNoRowsAffected = errors.New("no rows affected")
)

// ErrUnexpectedType is an error returned when attempting to assert incorrect type
type ErrUnexpectedType struct {
	Expected interface{}
	Got      interface{}
}

// Error implements errors interface
func (e ErrUnexpectedType) Error() string {
	return fmt.Sprintf("expected typed: %T, got type: %T", e.Expected, e.Got)
}

// BadRequestDetails explains why a request was a bad one.
type BadRequestDetails struct {
	ErrorMessage string
	Field        string
	Explanation  string
}

// FailedPreconditionDetails explains why a request did not meet preconditions.
type FailedPreconditionDetails struct {
	ErrorMessage string
	Type         string
	Subject      string
	Description  string
}

//MustErrBadRequest returns a gRPC status for a bad request.
// This panics if unable to add bad request details.
func MustErrBadRequest(brd BadRequestDetails) error {
	st := status.New(codes.InvalidArgument, brd.ErrorMessage)
	var err error

	if brd.Field != "" || brd.Explanation != "" {
		fieldViolation := &errdetails.BadRequest_FieldViolation{
			Field:       brd.Field,
			Description: brd.Explanation,
		}

		details := &errdetails.BadRequest{FieldViolations: []*errdetails.BadRequest_FieldViolation{fieldViolation}}
		st, err = st.WithDetails(details)
		if err != nil {
			panic(err)
		}
	}

	return st.Err()
}

// MustErrFailedPrecondition returns a gRPC status for a valid request, but one which the system could not process.
// This panics if unable to add bad request details.
func MustErrFailedPrecondition(fpd FailedPreconditionDetails) error {
	st := status.New(codes.FailedPrecondition, fpd.ErrorMessage)
	var err error

	if fpd.Description != "" || fpd.Type != "" || fpd.Subject != "" {
		violation := &errdetails.PreconditionFailure_Violation{
			Description: fpd.Description,
			Type:        fpd.Type,
			Subject:     fpd.Subject,
		}

		details := &errdetails.PreconditionFailure{Violations: []*errdetails.PreconditionFailure_Violation{violation}}
		st, err = st.WithDetails(details)
		if err != nil {
			panic(err)
		}
	}

	return st.Err()
}

// IsErrNoRowsAffected is a helper function that returns true if an error is an ErrNoRowsAffected
// else returns false
func IsErrNoRowsAffected(err error) bool {
	err = errors.Cause(err)
	return err == ErrNoRowsAffected
}

// IsErrNoRows is a helper that returns true iff the error is a sql.ErrNoRows
func IsErrNoRows(err error) bool {
	err = errors.Cause(err)
	return err == sql.ErrNoRows
}

// IsErrAlreadyExists is a helper that returns true iff the error is a gRPC codes.AlreadyExists
func IsErrAlreadyExists(err error) bool {
	err = errors.Cause(err)
	st := status.Convert(err)
	return st.Code() == codes.AlreadyExists
}

// IsErrNotFound is a helper that returns true iff the error is a gRPC codes.NotFound
func IsErrNotFound(err error) bool {
	err = errors.Cause(err)
	st := status.Convert(err)
	return st.Code() == codes.NotFound
}
