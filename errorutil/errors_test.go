package errorutil

import (
	"database/sql"
	"fmt"
	"testing"

	"github.com/pkg/errors"
)

func TestErrIsNoRows(t *testing.T) {
	t.Parallel()

	if IsErrNoRows(nil) {
		t.Error("expected nil to be false, but was true")
	}

	if IsErrNoRows(fmt.Errorf("foo")) {
		t.Error("expected error to be false, but was true")
	}

	if !IsErrNoRows(sql.ErrNoRows) {
		t.Error("expected sql.ErrNoRows to be true, but was not")
	}

	if !IsErrNoRows(errors.Wrap(sql.ErrNoRows, "record not found")) {
		t.Error("expected sql.ErrNoRows to be true, but was not")
	}
}
