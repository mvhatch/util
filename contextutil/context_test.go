package contextutil

import (
	"context"
	"net/http/httptest"
	"testing"
)

func TestContextUtil_ContextId(t *testing.T) {
	t.Run("Should return context id and ok", shouldReturnIDAndOk)
	t.Run("Should return empty context id and not ok", shouldReturnEmptyIDAndNotOk)
}

func shouldReturnIDAndOk(t *testing.T) {
	t.Parallel()

	ctxID := "fooId"
	ctx := context.WithValue(context.TODO(), ID, ctxID)

	id, ok := ContextID(ctx)

	if !ok {
		t.Error("Expected context id to be present, but wasn't")
	}

	if id != ctxID {
		t.Errorf("Expected context id to be %s:, Got %s", ctxID, id)
	}
}

func shouldReturnEmptyIDAndNotOk(t *testing.T) {
	t.Parallel()

	id, ok := ContextID(context.TODO())

	if ok {
		t.Error("Expected context id to not be present, but was")
	}

	if id != "" {
		t.Errorf("Expected context id to be empty, Got %s", id)
	}
}

func TestContextUtil_MustContextId(t *testing.T) {
	t.Run("Should return context id when present", shouldReturnID)
	t.Run("Should panic when missing id", shouldPanicWhenMissingID)
}

func shouldReturnID(t *testing.T) {
	t.Parallel()

	ctxID := "foo_id"
	ctx := context.WithValue(context.TODO(), ID, ctxID)

	id := MustContextID(ctx)

	if id != ctxID {
		t.Errorf("Expected context id to be %s:, Got %s", ctxID, id)
	}
}

func shouldPanicWhenMissingID(t *testing.T) {
	t.Parallel()

	defer func() {
		err := recover()
		if err == nil {
			t.Error("Expected to panic, but did not")
		}
	}()

	MustContextID(context.TODO())
}

func TestContextUtil_ContextIdFromReq(t *testing.T) {
	t.Run("Should return context id and ok from a request", shouldReturnIDAndOkFromReq)
	t.Run("Should return empty context id and not ok from a request", shouldReturnEmptyIDAndNotOkFromReq)
}

func shouldReturnIDAndOkFromReq(t *testing.T) {
	t.Parallel()

	req := httptest.NewRequest("GET", "http://example.com/foo", nil)
	ctxID := "fooId"
	ctx := context.WithValue(req.Context(), ID, ctxID)
	req = req.WithContext(ctx)

	id, ok := ContextIDFromReq(req)

	if !ok {
		t.Error("Expected context id to be present, but wasn't")
	}

	if id != ctxID {
		t.Errorf("Expected context id to be %s:, Got %s", ctxID, id)
	}
}

func shouldReturnEmptyIDAndNotOkFromReq(t *testing.T) {
	t.Parallel()

	req := httptest.NewRequest("GET", "http://example.com/foo", nil)

	id, ok := ContextIDFromReq(req)

	if ok {
		t.Error("Expected context id to not be present, but was")
	}

	if id != "" {
		t.Errorf("Expected context id to be empty, Got %s", id)
	}
}

func TestContextUtil_MustContextIdFromReq(t *testing.T) {
	t.Run("Should return context id when present in request", shouldReturnIDFromReq)
	t.Run("Should panic when missing id in request", shouldPanicWhenMissingIDFromReq)
}

func shouldReturnIDFromReq(t *testing.T) {
	t.Parallel()

	req := httptest.NewRequest("GET", "http://example.com/foo", nil)
	ctxID := "foo_id"
	ctx := context.WithValue(req.Context(), ID, ctxID)
	req = req.WithContext(ctx)

	id := MustContextIDFromReq(req)

	if id != ctxID {
		t.Errorf("Expected context id to be %s:, Got %s", ctxID, id)
	}
}

func shouldPanicWhenMissingIDFromReq(t *testing.T) {
	t.Parallel()

	defer func() {
		err := recover()
		if err == nil {
			t.Error("Expected to panic, but did not")
		}
	}()

	req := httptest.NewRequest("GET", "http://example.com/foo", nil)
	MustContextIDFromReq(req)
}

func TestContextUtil_AddToContext(t *testing.T) {
	t.Parallel()

	req := httptest.NewRequest("GET", "http://example.com/foo", nil)
	ctxID := "foo_id"

	ctx := AddToContext(req.Context(), ID, ctxID)
	ctx = context.WithValue(ctx, ID, ctxID)

	retrievedID := ctx.Value(ID).(string)
	if retrievedID != ctxID {
		t.Errorf("Expected context id to be %s:, Got %s", ctxID, retrievedID)
	}
}
