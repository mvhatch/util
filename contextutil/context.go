// Package contextutil contains helpers for common context tasks
package contextutil

import (
	"context"
	"net/http"
	"strings"

	"gitlab.com/mvhatch/util/jwtutil"

	"github.com/google/uuid"
)

// ContextKey is a custom type to be used safely as a key in a request context's map
type ContextKey string

const (
	// ID is the request context key for the request id
	ID ContextKey = "context_id"
	// Key is the request context key for the context itself
	Key ContextKey = "context"
	// UserIDContextKey is the request context key for the user id
	UserIDContextKey ContextKey = "user_id"
	// UserContextKey is the request context key for the user
	UserContextKey ContextKey = "user"

	tokenContextKey ContextKey = "token"
)

// NewContextWithID creates a new context from a given context.
// The returned context will also have a unique id embedded.
func NewContextWithID(ctx context.Context) context.Context {
	return AddToContext(ctx, ID, uuid.New().String())
}

// ContextID returns the id of a context if present
func ContextID(ctx context.Context) (string, bool) {
	id, ok := ctx.Value(ID).(string)
	return id, ok
}

// MustContextID returns the id of a context or panics
func MustContextID(ctx context.Context) string {
	return ctx.Value(ID).(string)
}

// ContextIDFromReq returns the context id of a request if present
func ContextIDFromReq(r *http.Request) (string, bool) {
	rID, ok := r.Context().Value(ID).(string)
	return rID, ok
}

// MustContextIDFromReq returns the context id of a request or panics
func MustContextIDFromReq(r *http.Request) string {
	return r.Context().Value(ID).(string)
}

// AddToContext adds arbitrary key/value pair of data to a request context
func AddToContext(c context.Context, key, data interface{}) context.Context {
	return context.WithValue(c, key, data)
}

// AddTokenToContext is a helper method to add a JWT to a request context
func AddTokenToContext(ctx context.Context, t *jwtutil.Token) context.Context {
	return AddToContext(ctx, tokenContextKey, t)
}

// Token returns the current JWT in the request context if present
func Token(ctx context.Context) (*jwtutil.Token, bool) {
	token, ok := ctx.Value(tokenContextKey).(*jwtutil.Token)
	return token, ok
}

// UserID returns the userID in the context if present
func UserID(ctx context.Context) (string, bool) {
	userID, ok := ctx.Value(UserIDContextKey).(string)
	return userID, ok
}

// MustUserID returns the userID in the context or panics
func MustUserID(ctx context.Context) string {
	userID, ok := UserID(ctx)
	if !ok || strings.TrimSpace(userID) == "" {
		panic("userID missing from context")
	}
	return userID
}
