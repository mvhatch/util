// Package serviceutil contains common setup for http or gRPC services
package serviceutil

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"time"

	"gitlab.com/mvhatch/util/grpcutil"
	"gitlab.com/mvhatch/util/httputil"
	"gitlab.com/mvhatch/util/logutil"
	"gitlab.com/mvhatch/util/pubsubutil/pub"
	"gitlab.com/mvhatch/util/tracingutil"

	_ "github.com/jnewmano/grpc-json-proxy/codec" // import for side-effect of registering custom codec.
	"github.com/pkg/errors"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"google.golang.org/grpc"
)

const (
	defaultLogLevel      = logutil.InfoLevel
	defaultAddress       = ":8080"
	defaultHealthAddress = ":8081"
	defaultHTTPTimeout   = time.Duration(4000 * time.Millisecond)

	serviceNameEnvKey      = "SERVICE_NAME"
	serviceAddressEnvKey   = "SERVICE_ADDRESS"
	serviceVersionEnvKey   = "SERVICE_VERSION"
	logLevelEnvKey         = "LOG_LEVEL"
	healthAddressEnvKey    = "HEALTH_ADDRESS"
	servicePublisherEnvKey = "PUBSUB_PUBLISHER"
	gcpProjectIDEnvKey     = "GCP_PROJECT_ID"

	googlePubSub = "GOOGLE_PUBSUB"
)

var (
	// ErrNoVersion is returned when the service's version is not set in the env var
	ErrNoVersion = errors.New("service version not set")
	// ErrNoName is returned when the service's name is not set in the env var
	ErrNoName = errors.New("service name not set")
	// ErrNoAddress is return when the service's address is not set in the env var
	ErrNoAddress = errors.New("gRPC server host:port not set")
	// ErrNoTracerCloser is returned when non cleanup function for a tracingutil.tracer is set
	ErrNoTracerCloser = errors.New("tracer closer not set")
	// ErrNoConfig is returned when nil config cannot be validated
	ErrNoConfig = errors.New("nil config")
	// ErrUnknownPublisher is returned when unable to create a publisher based on the type name
	ErrUnknownPublisher = errors.New("unknown publisher")
)

// Service encapsulates the shared properties of a service
type Service struct {
	name          string
	address       string
	version       string
	logger        logutil.Logger
	tracer        tracingutil.Tracer
	tracerCloser  io.Closer
	publisher     pub.Publisher
	clientConns   map[string]*grpc.ClientConn
	healthAddress string // host:port of the /health endpoint
}

// ServiceConfig contains configuration settings for a service.
// These settings may be parsed from env vars
type ServiceConfig struct {
	Name          string
	Address       string
	Version       string
	Tracer        tracingutil.Tracer
	TracerCloser  io.Closer
	Logger        logutil.Logger
	Publisher     pub.Publisher
	HealthAddress string
}

// ServiceRegisterer is a function that enables the registering of a gRPC server and an arbitrary
// proto service registry
type ServiceRegisterer func(*grpc.Server)

// AddGRPCCLientConnectionOption configures a gRPC client connection.
type AddGRPCCLientConnectionOption struct {
	// ServerAddress is the host:port to the gRPC server a client needs a connection to
	ServerAddress string
	// ServiceName is the name of the gRPC service the client needs a connection to
	ServiceName string
}

// NewService attempts to create a generic service with defined properties.
// It errs if any configuration is invalid or missing.
func NewService(conf ServiceConfig) (*Service, error) {
	if err := validateConfig(&conf); err != nil {
		return nil, errors.Wrap(err, "could not create service")
	}

	if conf.Address == "" {
		return nil, errors.Wrap(ErrNoAddress, "could not create service")
	}

	if conf.HealthAddress == "" {
		conf.HealthAddress = defaultHealthAddress
	}

	if conf.Logger == nil {
		buildLogger(&conf, defaultLogLevel, logutil.KeyValFormatter)
	}

	if conf.Tracer == nil {
		if err := BuildTracer(&conf); err != nil {
			return nil, errors.Wrap(err, "could not create service")
		}
	}

	if conf.TracerCloser == nil {
		return nil, errors.Wrap(ErrNoTracerCloser, "could not create service")
	}

	s := Service{
		name:          conf.Name,
		address:       conf.Address,
		version:       conf.Version,
		logger:        conf.Logger,
		tracer:        conf.Tracer,
		tracerCloser:  conf.TracerCloser,
		publisher:     conf.Publisher,
		healthAddress: conf.HealthAddress,
	}

	return &s, nil
}

// ParseServiceEnv parses environment vars for basic service configuration settings.
// If a non-empty, invalid log level is set, this method returns an error.
func ParseServiceEnv() (ServiceConfig, error) {
	sc := ServiceConfig{
		Name:          os.Getenv(serviceNameEnvKey),
		Address:       os.Getenv(serviceAddressEnvKey),
		Version:       os.Getenv(serviceVersionEnvKey),
		HealthAddress: os.Getenv(healthAddressEnvKey),
	}

	if sc.Address == "" {
		sc.Address = defaultAddress
	}

	if sc.HealthAddress == "" {
		sc.HealthAddress = defaultHealthAddress
	}

	if err := BuildLoggerFromEnv(&sc); err != nil {
		return sc, errors.Wrap(err, "could not create service")
	}

	if err := BuildPublisherFromEnv(&sc); err != nil {
		return sc, errors.Wrap(err, "could not create service")
	}

	return sc, nil
}

// MustParseServiceEnv attempts to parse the environment for service configuration.
// It panics if there's an error.
func MustParseServiceEnv() ServiceConfig {
	conf, err := ParseServiceEnv()
	if err != nil {
		panic(err)
	}
	return conf
}

// AddGRPCClientConnection allows for adding a new grpc.ClientConn to a gRPC server.
// A gRPC client connection establishes some middleware for the client, one of which requires a tracingutil.tracer.
func (s *Service) AddGRPCClientConnection(opts AddGRPCCLientConnectionOption) (*grpc.ClientConn, error) {
	if opts.ServiceName == "" {
		return nil, errors.Wrap(ErrNoName, "could not add client connection")
	}

	clientConn, err := grpcutil.NewClientConnection(grpcutil.ClientConnectionOptions{
		ServiceName:    s.name,
		ServiceVersion: s.version,
		ServerAddress:  opts.ServerAddress,
		Tracer:         s.tracer,
	})
	if err != nil {
		return nil, errors.Wrap(err, "could not add client connection")
	}

	if s.clientConns == nil {
		s.clientConns = make(map[string]*grpc.ClientConn)
	}

	s.clientConns[opts.ServiceName] = clientConn
	return clientConn, nil
}

// MustAddGRPCClientConnection attempts to create a grpc client connection. It panics if there's any error.
func (s *Service) MustAddGRPCClientConnection(opts AddGRPCCLientConnectionOption) *grpc.ClientConn {
	conn, err := s.AddGRPCClientConnection(opts)
	if err != nil {
		panic(err)
	}
	return conn
}

// Name returns the service's name
func (s *Service) Name() string {
	return s.name
}

// Version returns the service's version
func (s *Service) Version() string {
	return s.version
}

// Logger returns the service's logger
func (s *Service) Logger() logutil.Logger {
	return s.logger
}

// Tracer returns the service's tracer
func (s *Service) Tracer() tracingutil.Tracer {
	return s.tracer
}

// MustPublisher returns a Publisher. It panics if publisher is nil, so only call this method
// if you expected a publisher to have been created.
func (s *Service) MustPublisher() pub.Publisher {
	if s.publisher == nil {
		panic("nil publisher")
	}
	return s.publisher
}

// BuildLoggerFromEnv attempts to build a logger and set it to the provided ServiceConfig.
// It returns an error if misconfigured.
func BuildLoggerFromEnv(sc *ServiceConfig) error {
	svcLogLevel := defaultLogLevel
	envLogLevel := os.Getenv(logLevelEnvKey)
	if envLogLevel != "" {
		lvl, err := logutil.MapLogLevel(envLogLevel)
		if err != nil {
			return errors.Wrap(err, "could not parse env")
		}
		svcLogLevel = lvl
	}

	buildLogger(sc, svcLogLevel, logutil.KeyValFormatter)
	return nil
}

// BuildTracer attempts to build a tracer or errs
func BuildTracer(sc *ServiceConfig) error {
	var err error
	sc.Tracer, sc.TracerCloser, err = tracingutil.NewJaegerTracer(sc.Name, tracingutil.WithTag("svc-version", sc.Version), tracingutil.WithTag("svc", sc.Name))
	if err != nil {
		return errors.Wrap(err, "could not create service")
	}

	return nil
}

// BuildPublisherFromEnv attempts to build a Publisher or errs
func BuildPublisherFromEnv(sc *ServiceConfig) error {
	publisherType := os.Getenv(servicePublisherEnvKey)
	if publisherType == "" {
		return nil
	}

	if publisherType == googlePubSub {
		publisher, err := pub.NewGooglePublisher(os.Getenv(gcpProjectIDEnvKey))
		if err != nil {
			return errors.Wrap(err, "could not create publisher")
		}
		sc.Publisher = publisher
		return nil
	}

	return errors.Wrap(ErrUnknownPublisher, fmt.Sprintf("unknown publisher type: %s", publisherType))
}

func (s *Service) runHealthListener(r *httputil.MuxRouter) {
	r.Handle("/metrics", promhttp.Handler()).Methods("GET")
	r.Handle("/health", httputil.HealthHandler).Methods("GET")
	log.Printf("health and metrics handler running on %s\n", s.healthAddress)
	log.Fatal(http.ListenAndServe(fmt.Sprintf("%s", s.healthAddress), r))
}

func validateConfig(conf *ServiceConfig) error {
	if conf == nil {
		return ErrNoConfig
	}

	if conf.Name == "" {
		return ErrNoName
	}

	if conf.Version == "" {
		return ErrNoVersion
	}

	return nil
}

func buildLogger(conf *ServiceConfig, logLevel logutil.LogLevel, formatter logutil.Formatter) {
	stdOutLogger := logutil.StdOutLogger{
		DefaultData: logutil.LogData{
			"svc-name":    conf.Name,
			"svc-version": conf.Version,
		},
		Lvl:       logLevel,
		Formatter: formatter}
	tracerLogger := logutil.TracerLogger{Lvl: logLevel}

	conf.Logger = logutil.NewMultiLogger(stdOutLogger, tracerLogger)
}
