package serviceutil

import (
	"testing"

	"gitlab.com/mvhatch/util/pubsubutil/pub"
	"gitlab.com/mvhatch/util/testingutil"

	"github.com/pkg/errors"
)

func TestNewService(t *testing.T) {
	t.Parallel()

	svc, err := NewService(ServiceConfig{
		Name:    "fooName",
		Version: "fooVersion",
		Address: "fooAddress",
	})

	if err != nil {
		t.Fatal(err)
	}

	if svc.name != "fooName" {
		t.Errorf("expected service name to be fooName, but was %s", svc.name)
	}

	if svc.address != "fooAddress" {
		t.Errorf("expected service address to be fooAddress, but was %s", svc.address)
	}

	if svc.version != "fooVersion" {
		t.Errorf("expected service version to be fooVersion, but was %s", svc.version)
	}
}

func TestNewService_Errs(t *testing.T) {
	t.Parallel()

	svc, err := NewService(ServiceConfig{})
	if errors.Cause(err) != ErrNoName {
		t.Errorf("expected ErrNoName, but was %v", err)
	}

	if svc != nil {
		t.Error("expected service to be nil, but was not")
	}

	svc, err = NewService(ServiceConfig{Name: "fooService"})
	if errors.Cause(err) != ErrNoVersion {
		t.Errorf("expected ErrNoVersion, but was %v", err)
	}

	if svc != nil {
		t.Error("expected service to be nil, but was not")
	}

	svc, err = NewService(ServiceConfig{Name: "fooService", Version: "fooVersion"})
	if errors.Cause(err) != ErrNoAddress {
		t.Errorf("expected ErrNoAddress, but was %v", err)
	}

	if svc != nil {
		t.Error("expected service to be nil, but was not")
	}
}

func TestService_MustPublisher(t *testing.T) {
	t.Parallel()

	svc := Service{publisher: pub.MockPublisher{}}
	svc.MustPublisher()
}

func TestService_MustPublisher_errs_when_nil_publisher(t *testing.T) {
	t.Parallel()

	var svc Service
	testingutil.AssertPanic(t, func() {
		svc.MustPublisher()
	})
}
