package serviceutil

import (
	"fmt"
	"log"
	"net/http"

	"gitlab.com/mvhatch/util/httputil"

	"github.com/pkg/errors"
)

// HTTPService is a Service for http-specific methods
type HTTPService struct {
	*Service
}

// NewHTTPService returns a pointer to an HTTPService or an error
func NewHTTPService(conf ServiceConfig) (*HTTPService, error) {
	svc, err := NewService(conf)
	if err != nil {
		return nil, errors.Wrap(err, "could not create new http service")
	}
	return &HTTPService{Service: svc}, nil
}

// NewHTTPServiceFromEnv convenient function to parse the env and then return an *HTTPService
func NewHTTPServiceFromEnv() (*HTTPService, error) {
	conf, err := ParseServiceEnv()
	if err != nil {
		return nil, errors.Wrap(err, "could not create new http service")
	}
	return NewHTTPService(conf)
}

// MustHTTPServiceFromEnv returns a *HTTPService or panics if environment is misconfigured.
func MustHTTPServiceFromEnv() *HTTPService {
	svc, err := NewHTTPServiceFromEnv()
	if err != nil {
		panic(err)
	}
	return svc
}

// Run will attempt to start an http server.
func (s *HTTPService) Run(handler http.Handler) error {
	defer s.tracerCloser.Close()

	go s.runHealthListener(httputil.NewRouter())

	log.Printf("%s version: %s\n", s.name, s.version)
	log.Printf("log level: %s\n", s.logger.Level())
	log.Printf("http server listening on %s\n", s.address)

	return http.ListenAndServe(fmt.Sprintf("%s", s.address), handler)
}
