package serviceutil

import (
	"testing"
)

func TestNewHTTPService(t *testing.T) {
	t.Parallel()

	svc, err := NewHTTPService(ServiceConfig{
		Name:    "fooName",
		Version: "fooVersion",
		Address: "fooAddress",
	})

	if err != nil {
		t.Fatal(err)
	}

	if svc.name != "fooName" {
		t.Errorf("expected service name to be fooName, but was %s", svc.name)
	}

	if svc.address != "fooAddress" {
		t.Errorf("expected service address to be fooAddress, but was %s", svc.address)
	}

	if svc.version != "fooVersion" {
		t.Errorf("expected service version to be fooVersion, but was %s", svc.version)
	}
}
