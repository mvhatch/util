package serviceutil

import (
	"fmt"
	"log"
	"net"

	"gitlab.com/mvhatch/util/grpcutil"
	serverMiddleware "gitlab.com/mvhatch/util/grpcutil/middleware/server"
	"gitlab.com/mvhatch/util/httputil"

	"github.com/grpc-ecosystem/go-grpc-prometheus"
	"github.com/pkg/errors"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

// GRPCService is a service for gRPC-specific methods
type GRPCService struct {
	*Service
}

// NewGRPCService returns a pointer to a GRPCService or an error.
func NewGRPCService(conf ServiceConfig) (*GRPCService, error) {
	svc, err := NewService(conf)
	if err != nil {
		return nil, errors.Wrap(err, "could not create gRPC service")
	}

	return &GRPCService{Service: svc}, nil
}

// NewGRPCServiceFromEnv convenient function to parse the env and return a *GRPCService.
func NewGRPCServiceFromEnv() (*GRPCService, error) {
	conf, err := ParseServiceEnv()
	if err != nil {
		return nil, errors.Wrap(err, "could not create gRPC service")
	}
	return NewGRPCService(conf)
}

// MustGRPCServiceFromEnv returns a *GRPCService or panics if misconfigured.
func MustGRPCServiceFromEnv() *GRPCService {
	svc, err := NewGRPCServiceFromEnv()
	if err != nil {
		panic(err)
	}
	return svc
}

// Run will attempt to start a gRPC server.
func (s *GRPCService) Run(sr ServiceRegisterer) error {
	listener, err := buildListener(s.address)
	if err != nil {
		return errors.Wrap(err, "could not start gRPC server")
	}
	defer listener.Close()
	go s.runHealthListener(httputil.NewRouter())

	grpc_prometheus.EnableHandlingTimeHistogram()

	grpcServer := grpc.NewServer(grpcutil.WithUnaryServerChain(
		serverMiddleware.ContextInterceptor(s.tracer),
		grpc_prometheus.UnaryServerInterceptor,
		serverMiddleware.RequestIDInterceptor(s.tracer),
		serverMiddleware.ErrorInterceptor(s.tracer, s.logger),
		serverMiddleware.RecoveryInterceptor(s.tracer, s.logger),
		serverMiddleware.RouteInterceptor(s.tracer, s.logger),
	))

	reflection.Register(grpcServer)
	grpc_prometheus.Register(grpcServer)
	sr(grpcServer)

	defer s.tracerCloser.Close()

	log.Printf("version: %s\n", s.version)
	log.Printf("log level: %s\n", s.logger.Level())
	log.Printf("%s grpc server listening on %s\n", s.name, s.address)

	return grpcServer.Serve(listener)
}

// AddMiddleWare registers a route path and its associated middleware function
func (s *GRPCService) AddMiddleWare(route string, middleware serverMiddleware.Middleware) {
	serverMiddleware.RouterMiddleware[route] = middleware
}

func buildListener(address string) (net.Listener, error) {
	lis, err := net.Listen("tcp", fmt.Sprintf("%s", address))
	if err != nil {
		return nil, fmt.Errorf("failed to listen on address: %s", err)
	}

	return lis, nil
}
