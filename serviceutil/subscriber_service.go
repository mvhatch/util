package serviceutil

import (
	"fmt"
	"log"
	"os"

	"gitlab.com/mvhatch/util/httputil"
	"gitlab.com/mvhatch/util/pubsubutil/sub"

	"github.com/pkg/errors"
)

const (
	subscriberTypeEnvKey = "PUBSUB_SUBSCRIBER"
)

var (
	// ErrUnknownSubscriber is returned when unable to create a subscriber based on the type name
	ErrUnknownSubscriber = errors.New("unknown subscriber")
)

// SubscriberService is a service that subscribes to pubsub events
type SubscriberService struct {
	*Service
	subscriber sub.Subscriber
}

// SubscriberServiceConfig configures a new SubscriberService
type SubscriberServiceConfig struct {
	ServiceConfig
	Subscriber sub.Subscriber
}

// NewSubscriberService returns a pointer to a SubscriberService or an error.
func NewSubscriberService(conf SubscriberServiceConfig) (*SubscriberService, error) {
	svc, err := NewService(conf.ServiceConfig)
	if err != nil {
		return nil, errors.Wrap(err, "could not create subscriber service")
	}

	return &SubscriberService{Service: svc, subscriber: conf.Subscriber}, nil
}

// NewSubscriberServiceFromEnv convenient function to parse the env and return a *SubscriberService.
func NewSubscriberServiceFromEnv() (*SubscriberService, error) {
	conf, err := ParseServiceEnv()
	if err != nil {
		return nil, errors.Wrap(err, "could not create subscriber service from environment")
	}
	sconf := SubscriberServiceConfig{ServiceConfig: conf}
	if err := BuildSubscriberFromEnv(&sconf); err != nil {
		return nil, errors.Wrap(err, "could not create subscriber service environment")
	}

	return NewSubscriberService(sconf)
}

// MustSubscriberServiceFromEnv returns a *SubscriberService or panics if misconfigured.
func MustSubscriberServiceFromEnv() *SubscriberService {
	svc, err := NewSubscriberServiceFromEnv()
	if err != nil {
		panic(err)
	}
	return svc
}

// MustSubscriber returns a sub.Subscriber. It panics if subscriber is nil, so only call this method
// if you expected a subscriber to have been created.
func (s SubscriberService) MustSubscriber() sub.Subscriber {
	if s.subscriber == nil {
		panic("nil subscriber")
	}
	return s.subscriber
}

// Run starts a health listener in a goroutine and blocks indefinitely.
func (s SubscriberService) Run() {
	go s.runHealthListener(httputil.NewRouter())
	log.Printf("version: %s\n", s.version)
	log.Printf("log level: %s\n", s.logger.Level())
	log.Printf("%s listening for events on %s\n", s.name, s.address)
	select {}
}

// BuildSubscriberFromEnv attempts to build a Publisher or errs
func BuildSubscriberFromEnv(sc *SubscriberServiceConfig) error {
	subscriberType := os.Getenv(subscriberTypeEnvKey)
	if subscriberType == googlePubSub {
		subscriber, err := sub.NewGoogleSubscriber(os.Getenv(gcpProjectIDEnvKey))
		if err != nil {
			return errors.Wrap(err, "could not build subscriber from environment")
		}
		sc.Subscriber = subscriber
		return nil
	}

	return errors.Wrap(ErrUnknownSubscriber, fmt.Sprintf("could not build subscriber from environment for type: %s", subscriberType))
}
